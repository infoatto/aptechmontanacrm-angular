import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

interface responsedata {
    status_code:number;
    Data:any
}

@Injectable({
  providedIn: 'root'
})
export class CommonService {

    constructor(
    	private http:HttpClient
    ) { }

    listInquiries(page:any,size:any,enquiryFilter:any){
        const formData = new FormData();
        formData.append('page',page.toString()); 
        console.log(page) 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid')
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'inquiryno' : enquiryFilter.inquiryno,
                'inquirydate' : enquiryFilter.inquirydate,
                "page":page.toString(),
                'academicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`listInquiries`,formData,httpOptions);
    }

    getAcademicYears()
    {
        return this.http.get<any>(environment.url+`getAcademicYears`);
    }

    getCategories()
    {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
            })
        }
        return this.http.post(environment.url+`getCategories`,formData,httpOptions);
    }

    getCourses(categoryid)
    {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'categoryid' : categoryid,
            })
        }
        return this.http.post(environment.url+`getCourses`,formData,httpOptions);
    }

    getCountries()
    {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
            })
        }
        return this.http.post(environment.url+`getCountries`,formData,httpOptions);
    }

    getState(countryid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'countryid' : countryid,
            })
        }
        return this.http.post(environment.url+`getState`,formData,httpOptions);
    }

    getCity(stateid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'stateid' : stateid,
            })
        }
        return this.http.post(environment.url+`getCities`,formData,httpOptions);
    }

    getInquiryNo(){

        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
            })
        }
        return this.http.post(environment.url+`getInquiryNo`,formData,httpOptions);
    }

    addEditEnquiryForm(formValues:any,enquiry_id:any){
        const formData = new FormData();
        // formData.append('inquiryid',enquiry_id);
        formData.append('student_first_name',formValues.student_first_name); 
        formData.append('student_last_name',formValues.student_last_name); 
        formData.append('country_id',formValues.country_id);
        formData.append('state_id',formValues.state_id);
        formData.append('city_id',formValues.city_id);
        formData.append('pincode',formValues.pincode);
        formData.append('father_name',formValues.father_name);
        formData.append('father_contact_no',formValues.father_contact_no);         
        formData.append('father_emailid',formValues.father_emailid);
        formData.append('inquiry_remark',formValues.inquiry_remark); 
        formData.append('inquiry_handled_by',formValues.inquiry_handled_by);  
        
        if(formValues.rectype == false){
            formValues.rectype = 'Inquiry';
            formValues.inquiryno = ''
            if(formValues.student_dob.formatted == undefined){
                formData.append('student_dob',formValues.student_dob);
            }
            else{
                console.log(2)
                formData.append('student_dob',formValues.student_dob.formatted);
            }
            if(formValues.gender == false || formValues.gender == undefined){
                formValues.gender = 'Female';
            }
            else{
                formValues.gender = "Male"; 
            }
            // if(formValues.inquirydate == undefined){
            //     var today = new Date();
            //     var dd1 = today.getDate();
            //     var mm1 = today.getMonth() + 1; //January is 0!

            //     var yyyy = today.getFullYear();
            //     if (dd1 < 10) {
            //         var dd = '0' + dd1;
            //     } 
            //     else{
            //         var dd = '' + dd1
            //     }
            //     if (mm1 < 10) {
            //         var mm = '0' + mm1;
            //     } 
            //     else{
            //         var mm = '' + mm1
            //     }
            //     formValues.inquirydate = dd + '-' + mm + '-' + yyyy;
            // }
            formData.append('gender',formValues.gender); 
            formData.append('age_year',formValues.age_year);
            formData.append('age_month',formValues.age_month);
            formData.append('category_id',formValues.category_id); 
            formData.append('course_id',formValues.course_id); 
            if(formValues.has_attended_preschool_before == false || formValues.has_attended_preschool_before == undefined){
                formValues.has_attended_preschool_before = 'No';
            }
            else{
                formValues.has_attended_preschool_before = 'Yes';
            }
            formData.append('has_attended_preschool_before',formValues.has_attended_preschool_before); 
            formData.append('preschool_name',formValues.preschool_name); 
            formData.append('present_address',formValues.present_address);          
            formData.append('father_education',formValues.father_education); 
            formData.append('father_profession',formValues.father_profession); 
            formData.append('mother_name',formValues.mother_name); 
            formData.append('mother_contact_no',formValues.mother_contact_no); 
            formData.append('mother_emailid',formValues.mother_emailid); 
            formData.append('mother_education',formValues.mother_education); 
            formData.append('mother_profession',formValues.mother_profession); 
            formData.append('how_know_about_school',formValues.how_know_about_school); 
            formData.append('how_know_about_other',formValues.how_know_about_other); 
            if(formValues.inquirydate.jsdate == undefined){
                if(formValues.inquirydate.date == undefined){
                    formData.append('inquirydate',formValues.inquirydate);
                } 
                else{
                     formValues.inquirydate = formValues.inquirydate.day + '-' + formValues.inquirydate.month + '-'+ formValues.inquirydate.date.year
                }
            }
            else{
                var date = new Date(formValues.inquirydate.jsdate),
                month = ("0" + (date.getMonth()+1)).slice(-2),
                day  = ("0" + date.getDate()).slice(-2);
                formData.append('inquirydate',day + '-' + month + '-'+ date.getFullYear());
                formValues.inquirydate = day + '-' + month + '-'+ date.getFullYear()
            }
            formData.append('academicyear',formValues.academicyear); 
        }
        else{
            formValues.rectype = 'Lead';
            enquiry_id = ''
        }
        console.log(enquiry_id)

        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'inquirydate': formValues.inquirydate,
                'academicyear' : formValues.academicyear,
                'rectype' : formValues.rectype,
                'leadid' : enquiry_id,
                'inquirystatus' : formValues.inquirystatus,
                'inquiryprogress' : formValues.inquiryprogress,
                'inquiryno' : formValues.inquiryno

            })
        }
        return this.http.post<any>(environment.url+`addEditInquiry`,formData,httpOptions);
    }

    getInquiryDetails(enquiryid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'inquiryid': enquiryid,
            })
        }
        return this.http.post(environment.url+`getInquiryDetails`,formData,httpOptions);
    }

    getInquiries(){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'academicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`getInquiries`,formData,httpOptions);
    }

    getEnrollmentNo(){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
            })
        }
        return this.http.post(environment.url+`getEnrollmentNo`,formData,httpOptions);
    }

    getCenterUserBatches(categoryid,courseid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'categoryid' : categoryid,
                'courseid' : courseid
            })
        }
        return this.http.post(environment.url+`getCenterUserBatches`,formData,httpOptions);
    }

    addEditAdmissionTab1(formValues:any,student_id:any){
        const formData = new FormData();
        formValues.dobyear = formValues.dobyear.toString();
        formValues.dobmonth = formValues.dobmonth.toString();
        formData.append('studentid',student_id); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        if(formValues.attendedpreschoolbefore == false || formValues.attendedpreschoolbefore == undefined){
            formValues.attendedpreschoolbefore = 'No';
        }
        else{
            formValues.attendedpreschoolbefore = 'Yes';
        }
        if(formValues.admissiondate.formatted == undefined){
            var today = new Date();
            var dd1 = today.getDate();
            var mm1 = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd1 < 10) {
                var dd = '0' + dd1;
            } 
            else{
                var dd = '' + dd1
            }
            if (mm1 < 10) {
                var mm = '0' + mm1;
            } 
            else{
                var mm = '' + mm1
            }
            formValues.admissiondate = dd + '-' + mm + '-' + yyyy;
        }
        else{
            formValues.admissiondate = formValues.admissiondate.formatted
        }
        if(formValues.programmestartdate.formatted == undefined){
            let programmestartdate = formValues.programmestartdate.date.day+'-'+formValues.programmestartdate.date.month+'-'+formValues.programmestartdate.date.year
            formValues.programmestartdate = programmestartdate.toString()
        }
        else{
            formValues.programmestartdate = formValues.programmestartdate.formatted
        }
        formValues.ageon = formValues.programmestartdate
        if(formValues.programmeenddate.formatted == undefined){
            let programmeenddate = formValues.programmeenddate.date.day+'-'+formValues.programmeenddate.date.month+'-'+formValues.programmeenddate.date.year
            formValues.programmeenddate = programmeenddate.toString()
        }
        else{
            formValues.programmeenddate = formValues.programmeenddate.formatted
        }
        console.log(formValues)
        console.log(formValues.programmeenddate)
        if(formValues.dob.date.day == undefined){
            formValues.dob = formValues.dob
        }
        else{
            let dob = formValues.dob.date.day+'-'+formValues.dob.date.month+'-'+formValues.dob.date.year
            formValues.dob = dob.toString()
        }
        // if(formValues.ageon.formatted == undefined){
        //     let ageon = formValues.ageon.date.day+'-'+formValues.ageon.date.month+'-'+formValues.ageon.date.year
        //     formValues.ageon = ageon.toString()
        // }
        // else{
        //     formValues.ageon = formValues.ageon.formatted
        // }
        formData.append('zoneid',atob(zoneid)); 
        formData.append('centerid',atob(centerid)); 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'inquiryid': formValues.inquiryid,
                'enrollmentno': formValues.enrollmentno,
                'academicyearid': formValues.academicyearid,
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'studentid':student_id,
                'categoryid': formValues.categoryid,
                'courseid': formValues.courseid,
                'batchid': formValues.batchid,
                'admissiondate': formValues.admissiondate,
                'programmestartdate': formValues.programmestartdate,
                'programmeenddate': formValues.programmeenddate,
                'studentfirstname': formValues.studentfirstname,
                'studentlastname': formValues.studentlastname,
                'dob': formValues.dob,
                'ageon': formValues.ageon,
                'dobyear': formValues.dobyear,
                'dobmonth': formValues.dobmonth,
                'nationality': formValues.nationality,
                'religion': formValues.religion,
                'mothertongue' : formValues.mothertongue,
                'otherlanguages' : formValues.otherlanguages,
                'attendedpreschoolbefore' : formValues.attendedpreschoolbefore,
                'preschoolname' : formValues.preschoolname
            })
        }
       return this.http.post<any>(environment.url+`addEditAdmissionTab1`,formData,httpOptions);
    }

    admissionTab2(formValues:any,student_id:any){
        const formData = new FormData();
        console.log(formValues)
        formData.append('studentid',student_id); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        // if(formValues.sibling1dob == undefined){
        //     formValues.sibling1dob = '00-00-0000' 
        //     formValues.sibling1school = '' 
        // }
        // if(formValues.sibling2dob == undefined){
        //     formValues.sibling2dob = '00-00-0000'
        //     formValues.sibling2school = ''
        // }
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'fathername': formValues.fathername,
                'fatherprof': formValues.fatherprof,
                'fatherlanguages': formValues.fatherlanguages,
                'fathernationality': formValues.fathernationality,
                'mothername': formValues.mothername,
                'motherprof': formValues.motherprof,
                'motherlanguages': formValues.motherlanguages,
                'mothernationality': formValues.mothernationality,
                'sibling1id' : formValues.sibling1id,
                'sibling2id' : formValues.sibling2id,
                // 'sibling1name': formValues.sibling1name,
                // 'sibling1dob': formValues.sibling1dob,
                // 'sibling1school': formValues.sibling1school,
                // 'dosibling2nameb': formValues.sibling2name,
                // 'sibling2dob': formValues.sibling2dob,
                // 'sibling2school': formValues.sibling2school
            })
        }
        return this.http.post<any>(environment.url+`admissionTab2`,formData,httpOptions);
    }

    getStudentsWithEnrollmentNo(enrollmentno){
        const formData = new FormData();
        console.log(enrollmentno)
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'enrollmentno' : enrollmentno
            })
        }
        return this.http.post<any>(environment.url+`getStudentsWithEnrollmentNo`,formData,httpOptions);
    }

    admissionTab3(formValues:any,student_id:any){
        const formData = new FormData();
        formData.append('studentid',student_id); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'presentaddress': formValues.presentaddress,
                'countryid': formValues.countryid,
                'stateid': formValues.stateid,
                'cityid': formValues.cityid,
                'pincode': formValues.pincode.toString(),
                'motherhomecontactno': formValues.motherhomecontactno.toString(),
                'motherofficecontactno': formValues.motherofficecontactno.toString(),
                'mothermobilecontactno': formValues.mothermobilecontactno.toString(),
                'motheremailid': formValues.motheremailid,
                'fatherhomecontactno': formValues.fatherhomecontactno.toString(),
                'fatherofficecontactno': formValues.fatherofficecontactno.toString(),
                'fathermobilecontactno': formValues.fathermobilecontactno.toString(),
                'fatheremailid': formValues.fatheremailid,
                'emergencycontactname': formValues.emergencycontactname,
                'emergencycontacttelno': formValues.emergencycontacttelno.toString(),
                'emergencycontactmobileno': formValues.emergencycontactmobileno.toString(),
                'emergencycontactrelationship': formValues.emergencycontactrelationship,
            })
        }
        return this.http.post<any>(environment.url+`admissionTab3`,formData,httpOptions);
    }

    admissionTab4(formValues:any,student_id:any){
        const formData = new FormData();
        formData.append('studentid',student_id); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        if(formValues.authperson1gender == false || formValues.authperson1gender == undefined){
            formValues.authperson1gender = 'Female';
        }
        else{
            formValues.authperson1gender = "Male"; 
        }
        if(formValues.authperson2gender == false || formValues.authperson2gender == undefined){
            formValues.authperson2gender = 'Female';
        }
        else{
            formValues.authperson2gender = "Male"; 
        }
        if(formValues.authperson3gender == false || formValues.authperson3gender == undefined){
            formValues.authperson3gender = 'Female';
        }
        else{
            formValues.authperson3gender = "Male"; 
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'authperson1': formValues.authperson1,
                'authperson1relation': formValues.authperson1relation,
                'authperson1gender': formValues.authperson1gender,
                'authperson2': formValues.authperson2,
                'authperson2relation': formValues.authperson2relation,
                'authperson2gender': formValues.authperson2gender,
                'authperson3': formValues.authperson3,
                'authperson3relation': formValues.authperson3relation,
                'authperson3gender': formValues.authperson3gender,
            })
        }
        return this.http.post<any>(environment.url+`admissionTab4`,formData,httpOptions);
    }

    listAdmissions(page:any,size:any,admissionFilter:any){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'enrollmentno' : admissionFilter.enrollmentno,
                "page":page.toString(),
                'academicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`listAdmissions`,formData,httpOptions);
    }

    getAdmissionTab1(studentid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        }
        return this.http.post(environment.url+`getAdmissionTab1`,formData,httpOptions);
    }
    getAdmissionTab2(studentid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        }
        return this.http.post(environment.url+`getAdmissionTab2`,formData,httpOptions);
    }
    getAdmissionTab3(studentid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        }
        return this.http.post(environment.url+`getAdmissionTab3`,formData,httpOptions);
    }
    getAdmissionTab4(studentid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        }
        return this.http.post(environment.url+`getAdmissionTab4`,formData,httpOptions);
    }

    getAlbums(page:any,size:any,albumFilter:any){
        let newpage = page.toString();
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'page':newpage,
            })
        }
        return this.http.post(environment.url+`getAlbums`,formData,httpOptions);
    }

    getAlbumsScroll(page){
        let pageScroll = page.toString();
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'page':pageScroll,
            })
        }
        return this.http.post(environment.url+`getAlbums`,formData,httpOptions);
    }

    getAlbumsImages(albumId){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'albumid': albumId,
            })
        }
        return this.http.post(environment.url+`getAlbumsImages`,formData,httpOptions);
    }

    deleteAlbumsImages(albumimageid,albumid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'userid':  usermasterid,
                'albumid': albumid,
                'albumimageid': albumimageid,
            })
        }
        return this.http.post(environment.url+`deleteAlbumsImages`,formData,httpOptions);
    }

    uploadAlbumImage(formValues:any,album_id:any){
        const formData = new FormData();
        console.log(formValues);
        formData.append('albumpic',formValues); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'albumid': album_id,
            })
        }
        return this.http.post<any>(environment.url+`uploadAlbumImage`,formData,httpOptions);
    }

    addEditAdmissionDocs(formValues:any,student_id:any){
        const formData = new FormData();
        console.log(formValues);
        formData.append('duly_filled_admission_form',formValues.duly_filled_admission_form); 
        formData.append('birth_certificate',formValues.birth_certificate); 
        formData.append('profile_pic',formValues.profile_pic); 
        formData.append('family_photo',formValues.family_photo); 
        formData.append('address_proof',formValues.address_proof); 
        formData.append('duly_filled_child_profile_form',formValues.duly_filled_child_profile_form); 

        formData.append('duly_filled_admission_form_value',formValues.duly_filled_admission_form); 
        formData.append('birth_certificate_value',formValues.birth_certificate); 
        formData.append('profile_pic_value',formValues.profile_pic); 
        formData.append('family_photo_value',formValues.family_photo); 
        formData.append('address_proof_value',formValues.address_proof); 
        formData.append('duly_filled_child_profile_form_value',formValues.duly_filled_child_profile_form); 
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
            })
        }
        return this.http.post<any>(environment.url+`addEditAdmissionDocs`,formData,httpOptions);
    }

    getAdmissionDocs(studentid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': studentid,
            })
        }
        return this.http.post(environment.url+`getAdmissionDocs`,formData,httpOptions);
    }

    getFeesLevel()
    {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
            })
        }
        return this.http.post(environment.url+`getFeesLevel`,formData,httpOptions);
    }

    getCenterFees(fees_level_id,category_id,course_id,feesselectiontype)
    {
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid' : atob(zoneid),
                'centerid' : atob(centerid),
                'acadmicyear' : academicyear,
                'feesselectiontype' : feesselectiontype,
                'categoryid' : category_id,
                'courseid' : course_id,
                'feeslevelid' : fees_level_id,
            })
        }
        return this.http.post(environment.url+`getCenterFees`,formData,httpOptions);
    }
    payAdmissionFees(formValues:any,student_id:any,collected_amount:any,remainig_amount:any,monthArray:any,installmentNo:any){
        const formData = new FormData();
        let fees_collected_amount = 0;  
        let fees_remainig_amount = 0;
        var date = new Date();
        var year = date.getFullYear();
        let isinstallment = ''
        let noofinstallment = ''
        let maxinstallmentallowed = ''
        if(formValues.noofinstallment == ''){
            fees_collected_amount = formValues.fees_amount_collected
            fees_remainig_amount = formValues.fees_remaining_amount
            isinstallment = 'No' 
            noofinstallment = ''
            maxinstallmentallowed= ''
        }
        else{
            for(let i = 0 ; i < installmentNo[1] ; i++){
                if(formValues.total_amount == undefined){
                    let installment_amnt =  (formValues.fees_total_amount/installmentNo[1])
                    formData.append('installment_amount['+i+']',installment_amnt.toString())
                }
                else{
                    let installment_amnt =  (formValues.total_amount/installmentNo[1])

                    formData.append('installment_amount['+i+']',installment_amnt.toString())
                }
                fees_collected_amount = fees_collected_amount + Number(collected_amount[i])
                fees_remainig_amount = fees_remainig_amount + Number(remainig_amount[i])
                formData.append('installment_due_date['+i+']','01-'+ monthArray[i]+'-'+year)
                formData.append('installment_collected_amount['+i+']', collected_amount[i])
                formData.append('installment_remaining_amount['+i+']', remainig_amount[i])
            }
            isinstallment = 'Yes' 
            noofinstallment = installmentNo[1].toString()
            maxinstallmentallowed = installmentNo[0]
        }
        if(formValues.feestype == false){
            formValues.feestype = 'Class';
        }
        else{
            formValues.feestype = 'Group'
        }
        formData.append('fees_total_amount',formValues.fees_total_amount);
        formData.append('fees_amount_collected',fees_collected_amount.toString());
        formData.append('fees_remaining_amount',fees_remainig_amount.toString());
        formData.append('fees_remark',formValues.fees_remark);
        formData.append('fees_approved_accepted_by_name',formValues.fees_approved_accepted_by_name);
        formData.append('payment_mode',formValues.payment_mode);
        formData.append('discount_amount',formValues.discount_amount);
        formData.append('total_amount',formValues.total_amount);
        if(formValues.payment_mode == 'Cheque'){
            formData.append('cheque_no',formValues.cheque_no);
        }
        if(formValues.payment_mode == 'Netbanking'){
            formData.append('transaction_id',formValues.transaction_id)
            
        }
        if(formValues.payment_mode == 'Cheque' || formValues.payment_mode == 'Netbanking'){
            if(formValues.transaction_date.formatted == undefined){
                var today = new Date();
                var dd1 = today.getDate();
                var mm1 = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd1 < 10) {
                    var dd = '0' + dd1;
                } 
                else{
                    var dd = '' + dd1
                }
                if (mm1 < 10) {
                    var mm = '0' + mm1;
                } 
                else{
                    var mm = '' + mm1
                }
                formValues.transaction_date = dd + '-' + mm + '-' + yyyy;
            }
            else{
                formValues.transaction_date = formValues.transaction_date.formatted
            }
            formData.append('transaction_date',formValues.transaction_date);
        }
        if(formValues.payment_mode == 'Cheque' || formValues.payment_mode == 'Netbanking'){
            formData.append('bank_name',formValues.bank_name);
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        // if(formValues.feepaymenttype == false || formValues.feepaymenttype == undefined){
        //     formValues.isinstallment = 'No';
        //     formValues.noofinstallment = '';
        //     formValues.maxinstallmentallowed = '';
            
        // }
        // else{
        //     formValues.isinstallment = "Yes"; 
        //     formValues.noofinstallment = '';
        //     formValues.maxinstallmentallowed = '';
        // }
        // console.log(student_id)
        console.log(formValues)
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'acadmicyear': academicyear,
                'feesid': formValues.fees_id,
                'feestype': formValues.feestype.toString(),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'categoryid': formValues.categoryid,
                'courseid': formValues.courseid,
                'batchid': formValues.batchid,
                'isinstallment': isinstallment,
                'noofinstallment': noofinstallment,
                'maxinstallmentallowed': maxinstallmentallowed,
            })
        }
        return this.http.post<any>(environment.url+`payAdmissionFees`,formData,httpOptions);
    }

    viewTimetables(page:any,size:any,timetableFilter:any){
        const formData = new FormData();
        formData.append('page',page.toString());  
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                "page":page.toString(),
            })
        }
        return this.http.post(environment.url+`viewTimetables`,formData,httpOptions);
    }

    viewTimetableDetails(timetable_id:any){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'timetableid' : timetable_id
            })
        }
        return this.http.post<any>(environment.url+`viewTimetableDetails`,formData,httpOptions);
    }

    viewDocumentFolders(page:any,size:any,docmentFilter:any){
        const formData = new FormData();
        formData.append('page',page.toString());  
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                "page":page.toString(),
            })
        }
        return this.http.post(environment.url+`viewDocumentFolders`,formData,httpOptions);
    }

    viewFolderDocuments(page:any,size:any,folder_id:any){
        const formData = new FormData();
        formData.append('page',page.toString());  
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'folderid' : folder_id,
                "page":page.toString(),
            })
        }
        return this.http.post<any>(environment.url+`viewFolderDocuments`,formData,httpOptions);
    }

    viewFolderDoc(docfolder_id:any){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'folderdocumentid' : docfolder_id
            })
        }
        return this.http.post<any>(environment.url+`viewFolderDoc`,formData,httpOptions);
    }

    viewTimeTableDoc(timetabledoc_id:any){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(currentUser.zone_id),
                'centerid': atob(currentUser.center_id),
                'timetabledocumentid' : timetabledoc_id
            })
        }
        return this.http.post<any>(environment.url+`viewTimeTableDoc`,formData,httpOptions);
    }

    getInquiryId(enquiryno){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid')
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'inquiryno' : enquiryno,
                'academicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`listInquiries`,formData,httpOptions);
    }

    getStudentList(categoryid,courseid,batchid,feestype){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid')
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyear = atob(academicyearid);
        if(feestype == false){
            feestype = 'Class'
        }
        else{
            feestype = 'Group'
            categoryid = ''
            courseid = ''
            batchid  = ''
        }
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'feesselectiontype' : feestype,
                'acadmicyear' : academicyear,
                'categoryid' : categoryid,
                'courseid' : courseid,
                'batchid' : batchid,
            })
        }
        return this.http.post(environment.url+`getStudentList`,formData,httpOptions);
    }

    getStudentListAttendence(categoryid:any,courseid:any,batchid:any,groupid:any,feestype:any,attendancedate:any){
        const formData = new FormData();
        console.log(groupid)
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let academicyearid = localStorage.getItem('academicyearid')
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyear = atob(academicyearid);
        if(feestype == false){
            feestype = 'Class'
            groupid = ''
        }
        else{
            feestype = 'Group'
            categoryid = ''
            courseid = ''
            batchid  = ''
        }
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'feesselectiontype' : feestype,
                'acadmicyear' : academicyear,
                'categoryid' : categoryid,
                'courseid' : courseid,
                'batchid' : batchid,
                'groupid' : groupid,
                'formarkattendance' : 'Yes',
                'attendancedate' : attendancedate
            })
        }
        return this.http.post(environment.url+`getStudentList`,formData,httpOptions);
    }

    getStudentId(enrollmentno){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'enrollmentno' : enrollmentno,
                'academicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`listAdmissions`,formData,httpOptions);
    }

    getStudentPaymentDetails(studentid,feestype,categoryid,courseid){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        if(feestype == false){
            feestype = 'Class'
        }
        else{
            feestype = 'Group'
        }
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid' : studentid,
                'zoneid': atob(zoneid),
                'centerid': atob(centerid),
                'categoryid' : categoryid,
                'courseid' : courseid,
                'feesselectiontype' : feestype,
                'acadmicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`getStudentPaymentDetails`,formData,httpOptions);
    }
    payAdmissionRemainingFees(formValues:any,student_id:any,admissionfessid,installmentNo:any,collected_amount:any,remainig_amount:any,installment_amount:any){
        const formData = new FormData();
        if(formValues.feepaymenttype == false){
        }
        else{
            for(let i = 0 ; i < Number(installmentNo) ; i++){
                if(collected_amount[i] != undefined){
                    formData.append('admission_fees_instalment_id['+i+']',installment_amount[i])
                    formData.append('installment_collected_amount['+i+']', collected_amount[i])
                }

         
            }
        }
        formData.append('fees_approved_accepted_by_name',formValues.fees_approved_accepted_by_name);
        formData.append('fees_remark',formValues.fees_remark);
        formData.append('payment_mode',formValues.payment_mode);
        if(formValues.payment_mode == 'Cheque'){
            formData.append('cheque_no',formValues.cheque_no);
        }
        if(formValues.payment_mode == 'Netbanking'){
            formData.append('transaction_id',formValues.transaction_id);
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'studentid': student_id,
                'admissionfeesid': admissionfessid,
            })
        }
        return this.http.post<any>(environment.url+`payAdmissionRemainingFees`,formData,httpOptions);
    }

    getILeads(){
        const formData = new FormData();
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'academicyear' : academicyear
            })
        }
        return this.http.post(environment.url+`getILeads`,formData,httpOptions);
    }

    getGroupsDD(){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let utoken=currentUser.utoken; 
        const httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Methods":"GET, POST",
                "Access-Control-Allow-Origin":"*",
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
            })
        }
        return this.http.post(environment.url+`getGroupsDD`,formData,httpOptions);
    }

    markattendence(formValues:any,student_array: any){
        const formData = new FormData();
        if(formValues.feesselectiontype == false){
            formValues.feesselectiontype = 'Class'
            formValues.groupid = ''
        }
        else{
            formValues.feesselectiontype = 'Group'
            formValues.categoryid = ''
            formValues.courseid = ''
            formValues.batchid = ''
        }
        for(let i=0;i<student_array.length;i++){
            console.log(student_array)
            formData.append('student_id['+i+']',student_array[i])
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        let zoneid=currentUser.zone_id;
        let centerid=currentUser.center_id;
        let utoken=currentUser.utoken; 
        let academicyearid = localStorage.getItem('academicyearid')
        let academicyear = atob(academicyearid);
        const httpOptions = {
            headers: new HttpHeaders({
                'utoken':  atob(utoken),
                'userid': atob(usermasterid),
                'zoneid' : atob(zoneid),
                'centerid' : atob(centerid),
                'acadmicyear' : academicyear,
                'feesselectiontype' : formValues.feesselectiontype,
                'categoryid' : formValues.categoryid,
                'courseid' : formValues.courseid,
                'batchid' : formValues.batchid,
                'groupid' : formValues.groupid,
                'attendancedate' : formValues.attendancedate
            })
        }
        return this.http.post<any>(environment.url+`markAttendance`,formData,httpOptions);
    }

}

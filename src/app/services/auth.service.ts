import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

interface Loginresponse {
  success: boolean;
  message: string;
  token: string;
  userId: number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
  	private myRoute: Router,
  	private http: HttpClient,
  ) { }
  sendToken(token: string) {
    localStorage.setItem("LoggedInUser", token)
  }
  getToken() {
    return localStorage.getItem("LoggedInUser")
  }
  isLoggedIn() {
    return localStorage.getItem("LoggedInUser") != null
  }
  login(username:string,password:string){
    const httpOptions = {
      headers: new HttpHeaders({
        'username':  username,
        'password': password
      })
    };
   return this.http.post<Loginresponse>(environment.url+`userLogin`, 
        "",httpOptions)
            .pipe(map(Loginresponse => {
                return Loginresponse;
            }));
            
    }
    
    userLogOut(){
        const formData = new FormData();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let usermasterid=currentUser.user_id;
        const httpOptions = {
            headers: new HttpHeaders({
                'userid': atob(usermasterid),
            })
        }
        return this.http.post<any>(environment.url+`userLogOut`,formData,httpOptions);
    }
}

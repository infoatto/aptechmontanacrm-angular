import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { AuthService } from './services/auth.service';
import { AuthGuard } from './auth/auth.guard';
import { PdfViewerComponent } from 'ng2-pdf-viewer';

import { AppRoutingModule } from './routing/app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { LayoutComponent } from './components/common/layout/layout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SideNavComponent } from './components/common/side-nav/side-nav.component';
import { TopNavComponent } from './components/common/top-nav/top-nav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as $ from 'jquery';
import { ConstantComponent } from './components/constant/constant.component';
import { EnquirylistComponent } from './components/enquiry/enquirylist/enquirylist.component';
import { AddeditenquiryComponent } from './components/enquiry/addeditenquiry/addeditenquiry.component';
import {MatSnackBarModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Admissiontab1Component } from './components/admission/admissiontab1/admissiontab1.component';
import { AdmissionmasterComponent } from './components/admission/admissionmaster/admissionmaster.component';
import {MatTabsModule} from '@angular/material/tabs';
import { AlbumlistComponent } from './components/album/albumlist/albumlist.component';
import { AlbumimagesComponent } from './components/album/albumimages/albumimages.component';
import { TimetablemasterComponent } from './components/timetable/timetablemaster/timetablemaster.component';
import { TimetabledetailsComponent } from './components/timetable/timetabledetails/timetabledetails.component';
import { PdfviewComponent } from './components/timetable/pdfview/pdfview.component';
import { FolderdocumentComponent } from './components/folderdocuments/folderdocument/folderdocument.component';
import { DocumentdetailsComponent } from './components/folderdocuments/documentdetails/documentdetails.component';
import { PdfdocviewComponent } from './components/folderdocuments/pdfdocview/pdfdocview.component';
import { FeesdetailsComponent } from './components/remainingfees/feesdetails/feesdetails.component';
import { LSelect2Module } from 'ngx-select2';
import { Select2Directive } from './directives/select2.directive';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material';
import { Select2Module } from 'ng2-select2';
import { RemainingfeesComponent } from './components/admission/remainingfees/remainingfees.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MarkattendenceComponent } from './components/attendence/markattendence/markattendence.component';
import { NgDatepickerModule } from 'ng2-datepicker';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    LayoutComponent,
    DashboardComponent,
    SideNavComponent,
    TopNavComponent,
    ConstantComponent,
    EnquirylistComponent,
    AddeditenquiryComponent,
    Admissiontab1Component,
    AdmissionmasterComponent,
    AlbumlistComponent,
    AlbumimagesComponent,
    TimetablemasterComponent,
    TimetabledetailsComponent,
    PdfViewerComponent,
    PdfviewComponent,
    FolderdocumentComponent,
    DocumentdetailsComponent,
    PdfdocviewComponent,
    FeesdetailsComponent,
    Select2Directive,
    RemainingfeesComponent,
    MarkattendenceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    NgxMyDatePickerModule.forRoot(),
    MatTabsModule,
    LSelect2Module,
    MatAutocompleteModule,
    MatInputModule,
    Select2Module,
    BsDatepickerModule.forRoot(),
    MatDatepickerModule,
    NgDatepickerModule
  ],
  providers: [AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

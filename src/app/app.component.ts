import { Router, NavigationStart } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'components/common/layout/layout.component.html',
  styleUrls: ['components/common/layout/layout.component.css'],
})
export class AppComponent implements OnInit {
  title = 'aptechmontanacrm-angular';
  
  showHead: boolean = false;
  constructor(private router: Router){
  	router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] == '/login') {
          this.showHead = false;
        } else {
          this.showHead = true;
        }
      }
    });

  }

  ngOnInit(){
  }
}

import { Directive, EventEmitter, ElementRef, AfterViewInit, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { NgControl } from '@angular/forms';

declare var $: any;

@Directive({
  selector: '[appSelect2]'
})
export class Select2Directive {

	select2: any;

  @Input()
  dataUrl: string;

  @Input()
  placeholder: string;

  @Output()
  itemSelected = new EventEmitter<boolean>();

  constructor(private el: ElementRef, private control: NgControl) {

  }

  ngAfterViewInit(): void {
    if (this.dataUrl) {
      this.initializeSelect2();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const dataUrl = changes.dataUrl;

    // There are instances that at first dataUrl are not available.
    if (dataUrl.currentValue) {
      this.initializeSelect2();
    }
  }

  // TODO: Use select2 transport option instead of ajax
  initializeSelect2(): void {

    $('.select2').on('select2:select', (event) => {
       // Code you want to execute when you hit enter in select2 input box   
   });
  }

  clear() {
    if (this.select2) {
      this.select2.val(null).trigger('change');
      this.control.control.setValue(null);
    }
  }	

}

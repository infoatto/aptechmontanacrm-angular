import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { SideNavComponent } from '../components/common/side-nav/side-nav.component';
import { LoginComponent } from '../components/login/login.component';
import{AuthGuard} from '../auth/auth.guard';
import { EnquirylistComponent } from '../components/enquiry/enquirylist/enquirylist.component';
import { AddeditenquiryComponent } from '../components/enquiry/addeditenquiry/addeditenquiry.component';
import { AdmissionmasterComponent } from '../components/admission/admissionmaster/admissionmaster.component';
import { Admissiontab1Component } from '../components/admission/admissiontab1/admissiontab1.component';
import { RemainingfeesComponent } from '../components/admission/remainingfees/remainingfees.component';
import { AlbumlistComponent } from '../components/album/albumlist/albumlist.component';
import { AlbumimagesComponent } from '../components/album/albumimages/albumimages.component';
import { TimetablemasterComponent } from '../components/timetable/timetablemaster/timetablemaster.component';
import { TimetabledetailsComponent } from '../components/timetable/timetabledetails/timetabledetails.component';
import { PdfviewComponent } from '../components/timetable/pdfview/pdfview.component';
import { FolderdocumentComponent } from '../components/folderdocuments/folderdocument/folderdocument.component';
import { DocumentdetailsComponent } from '../components/folderdocuments/documentdetails/documentdetails.component';
import { PdfdocviewComponent } from '../components/folderdocuments/pdfdocview/pdfdocview.component';
import { MarkattendenceComponent } from '../components/attendence/markattendence/markattendence.component';

const routes: Routes = [
    {
        path:"login",
        component:LoginComponent,
    },
    {
        path:"",
        component:DashboardComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"enquirylist",
        component:EnquirylistComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"enquirylist/addeditenquiry",
        component:AddeditenquiryComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'enquirylist/addeditenquiry/:enquiryid',
        component: AddeditenquiryComponent,
        canActivate: [AuthGuard],
   },
   {
        path:"admissionlist",
        component:AdmissionmasterComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"admissionlist/addeditadmission",
        component:Admissiontab1Component,
        canActivate: [AuthGuard],
    },
    {
        path:"remainingfees",
        component:RemainingfeesComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'admissionlist/addeditadmission/:studentid',
        component: Admissiontab1Component,
        canActivate: [AuthGuard],
   },
   {
        path:"albumlist",
        component:AlbumlistComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'albumlist/albumimages/:albumid',
        component: AlbumimagesComponent,
        canActivate: [AuthGuard],
   },
   {
        path:"timetables",
        component:TimetablemasterComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"timetables/viewTimetable/:timetableid",
        component:TimetabledetailsComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"timetables/viewTimetable/:timetableid/:timetabledocid",
        component:PdfviewComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"documentfolder",
        component:FolderdocumentComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"documentfolder/viewDocument/:folderid",
        component:DocumentdetailsComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"documentfolder/viewDocument/:folderid/:docfolderid",
        component:PdfdocviewComponent,
        canActivate: [AuthGuard],
    },
    {
        path:"markattendence",
        component:MarkattendenceComponent,
        canActivate: [AuthGuard],
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

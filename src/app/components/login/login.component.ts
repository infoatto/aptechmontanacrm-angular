import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { ConstantComponent } from '../constant/constant.component';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	private classes = ['hold-transition', 'login-page'];
	public loginForm: FormGroup;
	loading = false;
	submitted = false;
	public defaultLogo = ConstantComponent.defaultLogo;
	massage="";
	constructor(
		private formBuilder: FormBuilder,
		private authService: AuthService,
		private router: Router,
	) { }
	private initForm(): void {
		this.loginForm = this.formBuilder.group({
		username: ['', Validators.required],
		password: ['', Validators.required]
	});
	}

 get f() { return this.loginForm.controls; }

onSubmit() 
{

	this.submitted = true;
	if (this.loginForm.invalid) {
			return;
	}
	else{
		this.authService.sendToken(this.f.username.value)
	}
	//this.loading = true;
	this.authService.login(this.f.username.value, this.f.password.value).subscribe((response:any)=>{
	if(response.status_code=200)
	{
		if(response.body)
		{
			if (response) 
			{   
				var user_details=
				{
					"utoken":btoa(response.body.utoken),
					"zone_id":btoa(response.body.zone_id),
					"center_id":btoa(response.body.center_id),
					"user_id":btoa(response.body.user_id),
					"first_name" : response.body.first_name,
					"last_name" : response.body.last_name, 

				}
				localStorage.setItem('currentUser', JSON.stringify(user_details));
				localStorage.setItem('academicyearid', btoa(response.body.academic_year));
				this.massage = response.msg;
				this.router.navigate(['']);
			}
		}
		else
		{
			this.massage = response.msg;
		}
	}
	else
	{
		this.massage = response.msg;
	}
});


}


	ngOnInit() {
		if(this.authService.isLoggedIn())
		{
			this.router.navigate([""]);
		}
		else{
			this.router.navigate(["login"]);
		}
		this.initForm();
		const body = document.getElementsByTagName('body')[0];
		for (const cl of this.classes) {
			body.classList.add(cl);
		}
	}
}

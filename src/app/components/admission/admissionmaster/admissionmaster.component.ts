import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-admissionmaster',
  templateUrl: './admissionmaster.component.html',
  styleUrls: ['./admissionmaster.component.css']
})
export class AdmissionmasterComponent implements OnInit {
	
	page = 0;
	admissionList:any = [];
	totalRecords:any = 0;
	size = 10;
	admissionFilter: any = {
    	enrollmentno:"",
	};
    isLoading = true

	constructor(
    	private commonService:CommonService,
    	private snackbar:MatSnackBar,
    ) { }

	ngOnInit() {
        localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
        localStorage.removeItem('category')
        localStorage.removeItem('course')
    	this.listAdmissions(1,this.admissionFilter);
	}

	listAdmissions(page: number,admissionFilter){

        this.commonService.listAdmissions((page - 1) ,this.size,admissionFilter).subscribe((response) => {
        	this.isLoading = false
            if(response['status_code'] == 200){
                this.admissionList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page=page
            }else{
               this.snackbar.open(response['msg'], '', {
                 duration : 500,
                  verticalPosition:"top",
                  panelClass: ['bg-red']
                });

                this.admissionList = [];
                this.totalRecords = 0 ;
            }
        },
        (err: any) => console.log("error",err),() =>
            console.log("getCustomersPage() retrieved customers for page:", + page)
        );
    }

    clearfilter()
    {
        this.admissionFilter.enrollmentno=""
        this.listAdmissions(this.page,this.admissionFilter)
    }

    onSubmit(){
        this.listAdmissions(this.page,this.admissionFilter)
    }

    pageChanged(event)
    {
        console.log(1)
        this.listAdmissions(event,this.admissionFilter)
    }

}

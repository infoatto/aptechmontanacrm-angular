import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionmasterComponent } from './admissionmaster.component';

describe('AdmissionmasterComponent', () => {
  let component: AdmissionmasterComponent;
  let fixture: ComponentFixture<AdmissionmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmissionmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit , NgZone} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';
import { Select2OptionData } from 'ng2-select2';
import { BsDatepickerConfig, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';

import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};





@Component({
  selector: 'app-admissiontab1',
  templateUrl: './admissiontab1.component.html',
  styleUrls: ['./admissiontab1.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class Admissiontab1Component implements OnInit {
	
	myOptions: INgxMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
    }

    date = new FormControl(moment());

	chosenYearHandler(normalizedYear: Moment,index) {
		const ctrlValue = this.date.value;
		ctrlValue.year(normalizedYear.year());
		this.AdmissionTab5Form.controls['date_'+index].setValue(ctrlValue);
		// this.date.setValue(ctrlValue);
	}

	chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>,index) {
		const ctrlValue = this.date.value;
		ctrlValue.month(normalizedMonth.month());
		this.AdmissionTab5Form.controls['date_'+index].setValue(ctrlValue);
		// this.date.setValue(ctrlValue);
		datepicker.close();
	}


    
    installment_due_date = ''
    months = [{'id':'01','value':'January'},{'id':'02','value':'February'},{'id':'03','value':'March'},{'id':'04','value':'April'},{'id':'05','value':'May'},{'id':'06','value':'June'},{'id':'07','value':'July'},{'id':'08','value':'August'},{'id':'09','value':'Sepetember'},{'id':'10','value':'October'},{'id':'11','value':'November'},{'id':'12','value':'December'}]
    monthId = ''
	ngpayment_mode : any
    payment_mode = ['Cheque', 'Cash', 'Netbanking'];
   	payment_mode1 : any
   	selectedOptions = [];
	optionsMap = {
        Cheque: false,
        Cash: false,
        Netbanking: false,
	};
	optionsChecked = [];
    admissionFilter: any = {
    	enrollmentno:"",
	};
	size = 10;
	page = 0;
	admissionFormSelFile = ""
	birthCerySelFile = ""
	profilePicSelFile = ""
	familyPhotoSelFile = ""
	addressProofSelFile = ""
	profileFormSelFile = ""

	public AdmissionTabForm : FormGroup;
	public AdmissionTab1Form: FormGroup;
	public AdmissionTab2Form: FormGroup;
	public AdmissionTab3Form: FormGroup;
	public AdmissionTab4Form: FormGroup;
	public AdmissionTab5Form: FormGroup;
	AdmissionFormSubmitted = false;
	AdmissionForm1Submitted = false;
	AdmissionForm2Submitted = false;
	AdmissionForm3Submitted = false;
	AdmissionForm4Submitted = false;
	AdmissionForm5Submitted = false;
	admissiondate = ''
	admissiondate1 : any
	ngxadmissiondate : any  = {  
	    jsdate: new Date()  
	};  
	transaction_date = ''
	transaction_date1 : any
	ngxtransactiondate : any  = {  
	    jsdate: new Date()  
	};  
	academicyears : any
	academicyearid = ""
	enrollmentno = "";
	categoryId = ""
	courseId = ""
	public inquiries: Array<Select2OptionData>;
	category : any
	courses : any
	batchid = ""
	batchId = ""
	batches : any
	programmestartdate = ""
	programmestartdate1 : any
	ngxprogramstartdate : any
	programmeenddate = ''
	programmeenddate1 : any
	ngxprogramenddate : any
	studentfirstname = ""
	studentlastname = ""
	dob : any
	dob1 : any
	ngxstudentdob : any
	ageon : any
	ngxageon : any
	nationality = ""
	religion = ""
	mothertongue = ""
	otherlanguages = ""
	attendedpreschoolbefore : boolean
    preschoolname = ""
    selectedIndex = 0
	fathername = "";
	mothername = "";
	fatherprof = '';
	motherprof = '';
	fatherlanguages : any
	motherlanguages : any
	fathernationality = '';
	mothernationality = ""
	sibling1name = ""
	sibling2name = ""
	sibling1id = ""
	sibling2id = ""
	// sibling1dob : any
	// sibling1dob1 : any
	// ngxsibling1dob : any
	// sibling2dob : any
	// sibling2dob1 : any
	// ngxsibling2dob : any
	// sibling1school = "";
	// sibling2school = "";
	presentaddress = "";
	countryId = "";
	countries : any
	states : any
	cities : any
	stateId = ""
	cityId = ""
	pincode = ''
	fathermobilecontactno = ""
	mothermobilecontactno = ""
	fatherhomecontactno = ''
	motherhomecontactno = ""
	fatherofficecontactno = ""
	motherofficecontactno = ""
	emergencycontactname = ""
	emergencycontactrelationship = ''
	emergencycontactmobileno = ''
	emergencycontacttelno = ""
	inquiryId : any
	studentId = ""
	dobyear : any
	dobmonth : any
	fatheremailid = ""
	motheremailid = ""
	authperson1 = "";
	authperson1relation = ''
	authperson1gender : boolean
	authperson2 = "";
	authperson2relation = ''
	authperson2gender : boolean
	authperson3 = "";
	authperson3relation = ''
	authperson3gender : boolean
	admissionForm1disable : boolean = true
	admissionForm2disable : boolean = true
	admissionForm3disable : boolean = true
	admissionForm4disable : boolean = true
	admissionForm5disable : boolean = true
	ageon1 : any
	selectedindex = 0
	duly_filled_admission_form = ""
	birth_certificate = ""
	profile_pic = ""
	family_photo = ""
	address_proof = ""
	duly_filled_child_profile_form = ""
	profile_pic_path = ''
	family_photo_path = ""
	admission_form_path = ""
	birth_certificate_path = ""
	address_proof_path = ''
	profile_form_path = ""
    feestype :boolean = false
    feesLevels : any
    fees_total_amount : any
    fees_id = ""
    feepaymenttype :boolean = false
    fees_amount_collected = ''
    fees_remaining_amount = ''
    fees_remark = ""
    fees_approved_accepted_by_name = ""
    cheque_no = ""
    bank_name = ""
    transaction_id = ""
    noofinstallment : any
    maxinstallmentallowed = ''
    installment = []
    installment_amount : any
    installment_collected_amount = ''
    installment_remaining_amount = 0
    Fees : any
    isLoading = false
    displaypayFees = true
    Enquirydisable = true
    admissiondateDisable = false
    EnquirydisableCreate = false
    optionsSelect: Select2Options;
    inquiries1 = ''
    inquiry_no_edit = ''
    getEnquiry : any
    studentidpass = ''
    index : any
    discount_amount : any
    total_amount : any
	sValue: Date = new Date(2017, 7);
 	minMode: BsDatepickerViewMode = 'month';
 
	bsConfig: Partial<BsDatepickerConfig>;


	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
		private snackbar:MatSnackBar,
		private router: Router,
        private zone: NgZone,
	) { }

	ngOnInit() {
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.fees_approved_accepted_by_name =  currentUser.first_name + ' ' + currentUser.last_name

		let student = localStorage.getItem('studentid')
		console.log(student)
		if(student == null){
			console.log(1)
		}
		else{
			this.isLoading = true
			this.Tab1(student)
			this.Tab2(student)
			this.Tab3(student)
			this.Tab4(student)
			this.Tab5(student)
			console.log('aaa')
			console.log(this.nationality)
			this.Enquirydisable = false
			this.EnquirydisableCreate = true
		}
		// this.index = localStorage.getItem('tabindex')
		// if(this.index == 1){
		// 	this.isLoading = true
		// 	this.admissionForm1disable = false
		// 	this.Tab1(student)
		// 	this.Tab2(student)
		// 	this.Tab3(student)
		// 	this.Tab4(student)
		// 	this.Tab5(student)
		// }
		// if(this.index == 2){
		// 	this.isLoading = true
		// 	this.admissionForm1disable = false
		// 	this.admissionForm2disable = false
		// 	this.Tab1(student)
		// 	this.Tab2(student)
		// 	this.Tab3(student)
		// 	this.Tab4(student)
		// 	this.Tab5(student)
		// }
		// if(this.index == 3){
		// 	this.isLoading = true
		// 	this.admissionForm1disable = false
		// 	this.admissionForm2disable = false
		// 	this.admissionForm3disable = false
		// 	this.Tab1(student)
		// 	this.Tab2(student)
		// 	this.Tab3(student)
		// 	this.Tab4(student)
		// 	this.Tab5(student)
		// }
		// if(this.index == 4){
		// 	this.isLoading = true
		// 	this.admissionForm1disable = false
		// 	this.admissionForm2disable = false
		// 	this.admissionForm3disable = false
		// 	this.admissionForm4disable = false
		// 	this.Tab1(student)
		// 	this.Tab2(student)
		// 	this.Tab3(student)
		// 	this.Tab4(student)
		// 	this.Tab5(student)
		// }
		// if(this.index == 5){
		// 	this.isLoading = true
		// 	this.admissionForm1disable = false
		// 	this.admissionForm2disable = false
		// 	this.admissionForm3disable = false
		// 	this.admissionForm4disable = false
		// 	this.admissionForm5disable = false
		// 	this.Tab1(student)
		// 	this.Tab2(student)
		// 	this.Tab3(student)
		// 	this.Tab4(student)
		// 	this.Tab5(student)
		// }
		if(localStorage.getItem('enquiryNO') == null){

		}
		else{
			this.getInquiryDetails(localStorage.getItem('enquiryNO'))
		}
	    this.optionsSelect = {
			placeholder: "Select Enquiry",
			allowClear: true,
			width: "100%",
		}

		this.academicyearid = '2'
		this.ngpayment_mode = 'Cash'
		this.AdmissionTabForm = this.formBuilder.group({
			inquiryid : [''],
 			admissiondate : ['',[Validators.required]],
			academicyearid : ['',[Validators.required]],
			enrollmentno : [''],
			categoryid : ['',[Validators.required]],
			courseid : ['',[Validators.required]],
			batchid : ['',[Validators.required]],
			programmestartdate : ['',[Validators.required]],
			programmeenddate : ['',[Validators.required]],		
			studentfirstname : ['',[Validators.required]],
			studentlastname : ['',[Validators.required]],
			dob : ['',[Validators.required]],
			ageon : [''],
			nationality : ['',[Validators.required]],
			religion : ['',[Validators.required]],
			mothertongue : ['',[Validators.required]],
			otherlanguages : [''],
			attendedpreschoolbefore : [''],
			preschoolname : [''],
			dobyear : [''],
			dobmonth : ['']
		})
		this.AdmissionTab1Form = this.formBuilder.group({	
			fathername : ['',[Validators.required]],
			mothername : ['',[Validators.required]],
			fatherprof : ['',[Validators.required]],
			motherprof : ['',[Validators.required]],
			fatherlanguages : ['',[Validators.required]],
			motherlanguages : ['',[Validators.required]],
			fathernationality : ['',[Validators.required]],
			mothernationality : ['',[Validators.required]],
			sibling1name : [''],
			sibling2name : [''],
			sibling1id : [''],
			sibling2id : [''],
			// sibling1dob : [''],
			// sibling2dob : [''],
			// sibling1school : [''],
			// sibling2school : [''],
			sibling1enrollment : [''],
			sibling2enrollment : [''],
		})
		this.AdmissionTab2Form = this.formBuilder.group({	
			presentaddress : ['',[Validators.required]],
			countryid : ['',[Validators.required]], 
			stateid : ['',[Validators.required]], 
			cityid : ['',[Validators.required]],  
			pincode : ['',[Validators.required]],
			fatheremailid : ['',[Validators.required]],
			motheremailid : [''	,[Validators.required]],
			fathermobilecontactno : ['',[Validators.required]], 
			mothermobilecontactno : ['',[Validators.required]], 
			fatherhomecontactno : [''],
			motherhomecontactno : [''],
			fatherofficecontactno : [''],
			motherofficecontactno : [''],
			emergencycontactname : ['',[Validators.required]],
			emergencycontactrelationship : ['',[Validators.required]],
			emergencycontactmobileno :  ['',[Validators.required]],
			emergencycontacttelno : ['']
		})
		this.AdmissionTab3Form = this.formBuilder.group({
			authperson1 : ['',[Validators.required]],
			authperson1relation : ['',[Validators.required]],
			authperson1gender : [''],
			authperson2 : [''],
			authperson2relation : [''],
			authperson2gender : [''],
			authperson3 : [''],
			authperson3relation : [''],
			authperson3gender : [''],
		})
		this.AdmissionTab4Form = this.formBuilder.group({
			duly_filled_admission_form : ['',[Validators.required]],
			birth_certificate : ['',[Validators.required]],
			profile_pic : ['',[Validators.required]],
			family_photo : ['',[Validators.required]],
			address_proof : ['',[Validators.required]],
			duly_filled_child_profile_form : ['',[Validators.required]],
		})
		this.AdmissionTab5Form = this.formBuilder.group({
			feestype : [''],
			fees_level : ['',[Validators.required]],
			discount_amount : [''],
			total_amount : [''],
			fees_total_amount : [''],
			fees_id : [''],
			feepaymenttype : [''],
			fees_amount_collected : [''],
			fees_remaining_amount : [''],
			fees_remark : ['',[Validators.required]],
			fees_approved_accepted_by_name : ['',[Validators.required]],
			payment_mode : ['',[Validators.required]],
			cheque_no : [''],
			bank_name : [''],
			transaction_id : [''],
			noofinstallment : [''],
			maxinstallmentallowed : [''],
			installment_collected_amount : [''],
			installment_remaining_amount : [''],
			installment_due_date : [''],
			fees : ['',[Validators.required]],
			transaction_date : [''],
			// date_0 : [''],
			// date_1 : ['']

		})
		// for(var i = 0; i < this.noofinstallment; i++) {
  //   <FormArray>this.AdmissionTab5Form.get('installment').push(new FormControl("date_"+i));
  // }
		this.getInquiries();
		this.getAcademicYears();
		this.getEnrollmentNo();
		this.getCategories();
		this.getCountries();
		this.getFeesLevel();

		this.route.params.subscribe(
			params => {
				if(this.route.snapshot.params.studentid)
				{
					this.Enquirydisable = false
					this.isLoading = true
					this.admissiondateDisable = true
	 				this.EnquirydisableCreate = true
					this.Tab1(this.route.snapshot.params.studentid)
					this.Tab2(this.route.snapshot.params.studentid)
					this.Tab3(this.route.snapshot.params.studentid)
					this.Tab4(this.route.snapshot.params.studentid)
					this.Tab5(this.route.snapshot.params.studentid)
					this.Tab6(this.route.snapshot.params.studentid)
					
				}
			}
		)
	}

	Tab1(studentid){
		this.commonService.getAdmissionTab1(studentid)
			.subscribe(
	 			data => {
	 			this.isLoading = false 
	 			if(data['status_code'] == 200){
		 			this.inquiryId = data['body'][0].inquiry_master_id
	 				this.commonService.getInquiryDetails(this.inquiryId).subscribe((response)=>{
						if(response['status_code'] == 200){
				 			this.inquiries1 = response['body'][0]['lead_no'][0]['inquiry_no'] + ' (' + data['body'][0].student_first_name + ' ' + data['body'][0].student_last_name + ')';
						}
						else{
						}
				    })
					this.admissiondate = data['body'][0].Student_details[0].admission_date; 
					this.admissiondate1=data['body'][0].Student_details[0].admission_date.split('-');
					this.ngxadmissiondate = {  
	                    date: {  
	                        year: parseInt(this.admissiondate1[2]),  
	                        month: parseInt(this.admissiondate1[1]),  
	                        day: parseInt(this.admissiondate1[0])
	                    },
	                }
		 			this.academicyearid = data['body'][0].Student_details[0].academic_year_id;
					this.enrollmentno=data['body'][0].enrollment_no;
					this.categoryId = data['body'][0].Student_details[0].category_id;
					this.getCourses(this.categoryId);
					this.courseId = data['body'][0].Student_details[0].course_id;
					localStorage.setItem('category',this.categoryId)
					localStorage.setItem('course',this.courseId)
					this.getCenterUserBatches(this.categoryId,this.courseId);
					this.batchId = data['body'][0].Student_details[0].batch_id;
					this.programmestartdate = data['body'][0].Student_details[0].programme_start_date; 
					this.programmestartdate1=data['body'][0].Student_details[0].programme_start_date.split('-');
					this.ngxprogramstartdate = {  
	                    date: {  
	                        year: parseInt(this.programmestartdate1[2]),  
	                        month: parseInt(this.programmestartdate1[1]),  
	                        day: parseInt(this.programmestartdate1[0])
	                    },
	                }
	                this.programmeenddate = data['body'][0].Student_details[0].programme_end_date;
					this.programmeenddate1=data['body'][0].Student_details[0].programme_end_date.split('-');
					this.ngxprogramenddate = {  
	                    date: {  
	                        year: parseInt(this.programmeenddate1[2]),  
	                        month: parseInt(this.programmeenddate1[1]),  
	                        day: parseInt(this.programmeenddate1[0])
	                    },
	                }
		 			this.studentfirstname = data['body'][0].student_first_name;
		 			this.studentlastname = data['body'][0].student_last_name;
		 			this.dob = data['body'][0].dob; 
					this.dob1=data['body'][0].dob.split('-');
					this.ngxstudentdob = {  
	                    date: {  
	                        year: parseInt(this.dob1[0]),  
	                        month: parseInt(this.dob1[1]),  
	                        day: parseInt(this.dob1[2])
	                    },
	                }
	                this.ageon = data['body'][0].age_on; 
					this.ageon1=data['body'][0].age_on.split('-');
					this.ngxageon = {  
	                    date: {  
	                        year: parseInt(this.ageon1[2]),  
	                        month: parseInt(this.ageon1[1]),  
	                        day: parseInt(this.ageon1[0])
	                    },
	                }
	                this.dobyear = data['body'][0].dob_year;
		 			this.dobmonth = data['body'][0].dob_month;
		 			this.nationality = data['body'][0].nationality;

					if(this.nationality != ''){
						console.log(1)
						this.admissionForm1disable = false
					}
		 			this.religion = data['body'][0].religion;
		 			this.mothertongue = data['body'][0].mother_tongue;
					this.otherlanguages = data['body'][0].other_languages;
					if(data['body'][0].has_attended_preschool_before == 'Yes'){
						this.attendedpreschoolbefore = true;
					}
					else{
						this.attendedpreschoolbefore = false;
					}
					this.preschoolname = data['body'][0].preschool_name;
					this.studentId=studentid;
					if(this.route.snapshot.params.studentid)
					{
						this.admissionForm1disable = false
						this.admissionForm2disable = false
						this.admissionForm3disable = false
						this.admissionForm5disable = false
					}
				}

			})
	}

	Tab2(studentid){
		this.commonService.getAdmissionTab2(studentid)
		.subscribe(
 			data => {
 			this.isLoading = false
 			if(data['status_code'] == 200){
	 			this.fathername = data['body'][0].father_name;
	 			if(this.fathername == ''){
	 				this.getInquiryDetails(localStorage.getItem('enquiryNO'))
	 			}
	 			this.mothername = data['body'][0].mother_name;
				this.fatherprof = data['body'][0].father_prof;
	 			this.motherprof = data['body'][0].mother_prof;
	 			this.fatherlanguages = data['body'][0].father_languages;
				if(this.fatherlanguages != ''){
					this.admissionForm2disable = false
				}
	 			this.motherlanguages = data['body'][0].mother_languages;
	 			this.fathernationality = data['body'][0].father_nationality;
	 			this.mothernationality = data['body'][0].mother_nationality;
	 			this.sibling1name = data['body'][0].sibling1_name;
	 			this.sibling2name = data['body'][0].sibling2_name;
	 			this.sibling1id = data['body'][0].sibling1_id;
	 			this.sibling2id = data['body'][0].sibling2_id;
	 		// 	this.sibling1name = data['body'][0].sibling1_name;
	 		// 	this.sibling2name = data['body'][0].sibling2_name;
	 		// 	this.sibling1dob = data['body'][0].sibling1_dob; 
	 		// 	if(data['body'][0].sibling1_dob != undefined){
				// this.sibling1dob1=data['body'][0].sibling1_dob.split('-');
				// 	this.ngxsibling1dob = {  
	   //                  date: {  
	   //                      year: parseInt(this.sibling1dob1[2]),  
	   //                      month: parseInt(this.sibling1dob1[1]),  
	   //                      day: parseInt(this.sibling1dob1[0])
	   //                  },
	   //              }
    //         	} 
	 		// 	if(data['body'][0].sibling2_dob != undefined){
	   //              this.sibling2dob = data['body'][0].sibling2_dob; 
				// 	this.sibling2dob1=data['body'][0].sibling2_dob.split('-');
				// 	this.ngxsibling2dob = {  
	   //                  date: {  
	   //                      year: parseInt(this.sibling2dob1[2]),  
	   //                      month: parseInt(this.sibling2dob1[1]),  
	   //                      day: parseInt(this.sibling2dob1[0])
	   //                  },
	   //              }
	   //          }
	 		// 	this.sibling1school = data['body'][0].sibling1_school;
	 		// 	this.sibling2school = data['body'][0].sibling2_school;
				this.studentId=studentid;
			}

		})
	}

	Tab3(studentid){
		this.commonService.getAdmissionTab3(studentid)
			.subscribe(
	 			data => {
	 			this.isLoading = false
	 			if(data['status_code'] == 200){
		 			this.presentaddress = data['body'][0].present_address;
		 			if(this.presentaddress == ''){
		 				this.getInquiryDetails(localStorage.getItem('enquiryNO'))
		 			}
		 			this.countryId = data['body'][0].country_id;
					this.getState(this.countryId);
					this.stateId = data['body'][0].state_id;
					this.getCity(this.stateId);
					this.cityId = data['body'][0].city_id;
					this.pincode = data['body'][0].pincode;
		 			this.fatheremailid = data['body'][0].father_email_id;
		 			this.motheremailid = data['body'][0].mother_email_id;
		 			this.fathermobilecontactno = data['body'][0].father_mobile_contact_no;
		 			this.mothermobilecontactno = data['body'][0].mother_mobile_contact_no;
		 			this.fatherhomecontactno = data['body'][0].father_home_contact_no;
		 			this.motherhomecontactno = data['body'][0].mother_home_contact_no;
		 			this.fatherofficecontactno = data['body'][0].father_office_contact_no;
		 			this.motherofficecontactno = data['body'][0].mother_office_contact_no;
		 			this.emergencycontactname = data['body'][0].emergency_contact_name;

					if(this.emergencycontactname != ''){
						this.admissionForm3disable = false
					}
		 			this.emergencycontactrelationship = data['body'][0].emergency_contact_relationship;
		 			this.emergencycontacttelno = data['body'][0].emergency_contact_tel_no;
		 			this.emergencycontactmobileno = data['body'][0].emergency_contact_mobile_no;
					this.studentId=studentid;
				}
			})
	}

	Tab4(studentid){
		this.commonService.getAdmissionTab4(studentid)
		.subscribe(
 			data => {
 			this.isLoading = false
 			if(data['status_code'] == 200){
	 			this.authperson1 = data['body'][0].auth_person1_to_collect;

				if(this.authperson1 != ''){
					this.admissionForm4disable = false
				}
				this.authperson1relation = data['body'][0].auth_person1_to_collect_relation; 
				if(data['body'][0].auth_person1_to_collect_gender == 'Male'){
					this.authperson1gender = true;
				}
				else{
					this.authperson1gender = false;
				}
				this.authperson2=data['body'][0].auth_person2_to_collect;
	 			this.authperson2relation = data['body'][0].auth_person2_to_collect_relation;
	 			if(data['body'][0].auth_person2_to_collect_gender == 'Male'){
					this.authperson2gender = true;
				}
				else{
					this.authperson2gender = false;
				}
	 			this.authperson3 = data['body'][0].auth_person3_to_collect; 
                this.authperson3relation = data['body'][0].auth_person3_to_collect_relation;
	 			if(data['body'][0].auth_person3_to_collect_gender == 'Male'){
					this.authperson3gender = true;
				}
				else{
					this.authperson3gender = false;
				}
				this.studentId=studentid;
			}
		})
	}

	Tab5(studentid){
		this.commonService.getAdmissionDocs(studentid)
		.subscribe(
 			data => {
 			this.isLoading = false
 			if(data['status_code'] == 200){
	 			this.admission_form_path = data['body'][0].duly_filled_admission_form_path;
				if(this.admission_form_path != ''){
					this.admissionForm5disable = false
				}
	 			this.birth_certificate_path =  data['body'][0].birth_certificate_path;
	 			this.profile_pic_path = data['body'][0].profile_pic_path;
	 			this.family_photo_path = data['body'][0].family_photo_path;
	 			this.address_proof_path = data['body'][0].address_proof_path;
	 			this.profile_form_path = data['body'][0].duly_filled_child_profile_form_path; 
				this.duly_filled_admission_form = data['body'][0].duly_filled_admission_form; 
				this.birth_certificate = data['body'][0].birth_certificate; 
				this.profile_pic = data['body'][0].profile_pic; 
				this.family_photo = data['body'][0].family_photo; 
				this.address_proof = data['body'][0].address_proof; 
				this.duly_filled_child_profile_form = data['body'][0].duly_filled_child_profile_form;

				if(this.authperson1 != ''){
					this.admissionForm4disable = false
				}
				this.studentId=studentid;
			}
		})
	}

	Tab6(studentid){
		let feestype = 'Class'
		let category = localStorage.getItem('category')
		let course = localStorage.getItem('course')
		this.commonService.getStudentPaymentDetails(studentid,feestype,category,course)
		.subscribe(
 			data => {
 			this.isLoading = false
 			if(data['status_code'] == 200){
 				console.log(1)
	 			this.displaypayFees = true
			}
			else{
				console.log(2)
				this.displaypayFees = false
			}
		})
	}

	getInquiries(){
		this.commonService.getInquiries().subscribe((response)=>{
			if(response['status_code']==200)
		    {
		    	var arr = [];
		    	var arr_id = [];
		    	for(var i=0; i<response['body'].length; i++) {
	                var no = response['body'][i].inquiry_no ;

	                var first_name = response['body'][i].student_first_name ;
	                var last_name = response['body'][i].student_last_name ;
	                arr.push(no + ' (' + first_name + ' ' + last_name +' )');
	             }
				this.inquiries = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	onadmissionDateChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.admissiondate = event.formatted
        }
    }

    ontransactionDateChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.transaction_date = event.formatted
        }
    }

    onprogramStartDateChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.programmestartdate = event.formatted
        }
        if(this.programmeenddate != ''){
        	let start = this.programmestartdate.split('-')
			let start_date = start[1]+'-'+start[0]+'-'+start[2]
			let end = this.programmeenddate.split('-')
			let end_date = end[1]+'-'+end[0]+'-'+end[2]
    		if(new Date(start_date) > new Date(end_date)){
    			this.snackbar.open('Program Start Date less than Program End Date', '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				}); 
				return
    		}
    		else{
        		this.calcDate(this.programmestartdate,this.dob);	
    		}
        }
        else{
        	this.calcDate(this.programmestartdate,this.dob);
        }
    }

    onprogramEndDateChanged(event: IMyDateModel): void 
    {
    	if(this.programmestartdate == ''){
    		this.snackbar.open('Select Program Start date', '', {
				duration: 500,
				verticalPosition:"top",
				panelClass: ['bg-red']
			}); 
			return
    	}
    	else{
    		if(event.formatted !== '') {
	            this.programmeenddate = event.formatted
	        }
	        let start = this.programmestartdate.split('-')
			let start_date = start[1]+'-'+start[0]+'-'+start[2]
			let end = this.programmeenddate.split('-')
			let end_date = end[1]+'-'+end[0]+'-'+end[2]
    		if(new Date(start_date) > new Date(end_date)){
    			this.snackbar.open('Program Start Date less than Program End Date', '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				}); 
				return
    		}
    		// else{
		    //     this.calcDate(this.programmeenddate,this.dob);	
    		// }
    	}
    }

    // onsiblingdob1Changed(event: IMyDateModel): void 
    // {
    //     if(event.formatted !== '') {
    //         this.sibling1dob = event.formatted
    //     }
    // }

    // onsiblingdob2Changed(event: IMyDateModel): void 
    // {
    //     if(event.formatted !== '') {
    //         this.sibling2dob = event.formatted
    //     }
    // }

    // onageOnChanged(event: IMyDateModel): void 
    // {
    //     if(event.formatted !== '') {
    //         this.ageon = event.formatted
    //     }
    //     this.calcDate(this.ageon,this.dob);
    // }

    calcDate(ageOn,dob){
    	if(ageOn != undefined && dob != undefined){
	    	var ageOnsplit = ageOn.split('-');
	    	var ageOndate = ageOnsplit[1]+'-'+ageOnsplit[0]+'-'+ageOnsplit[2];
	    	var dobsplit = dob.split('-');
	    	var firstsplitlength = dobsplit[0].length
	    	if(firstsplitlength == 4){
	    		var dobdate = dobsplit[1]+'-'+dobsplit[2]+'-'+dobsplit[0];	
	    	}
	    	else{
	    		var dobdate = dobsplit[1]+'-'+dobsplit[0]+'-'+dobsplit[2];
	    	}
	    	var diff = Math.abs(new Date(ageOndate).getTime() - new Date(dobdate).getTime());
		    var day = 1000 * 60 * 60 * 24;

		    var days = Math.ceil(diff/day);
		    this.dobmonth = Math.floor(days/30);
		    this.dobyear = Math.floor(this.dobmonth/12);
		    if(this.dobyear > 0){
		    	var yeardays = days - (365 * this.dobyear);
		    	let dobmonthfloor = Math.floor(yeardays/30);
		    	this.dobmonth = Math.abs(dobmonthfloor)
		    	// if(this.dobmonth < 0){
		    		
		    	// }
		    	// else{
		    	// 	this.dobmonth = Math.floor(yeardays/30);
		    	// }
		    }
	    }
    }

    getAcademicYears(){
        this.commonService.getAcademicYears().subscribe((response)=>{
        	if(response['status_code']==200)
		    {
				this.academicyears = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
        })
    }

    getEnrollmentNo(){
    	this.commonService.getEnrollmentNo().subscribe((response)=>{
            if(response['status_code']==200)
		    {
            	this.enrollmentno = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
        })
    }

    getCategories(){
	    this.commonService.getCategories().subscribe((response)=>{
	        if(response['status_code']==200)
		    {
	        	this.category = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCourses(categoryid){
	    this.commonService.getCourses(categoryid).subscribe((response)=>{
	    	if(response['status_code']==200)
		    {
	        	this.courses = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCenterUserBatches(categoryid,courseid){
		this.commonService.getCenterUserBatches(categoryid,courseid).subscribe((response)=>{
			if(response['status_code']==200)
		    {
	        	this.batches = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getFeesLevel(){
	    this.commonService.getFeesLevel().subscribe((response)=>{
	    	if(response['status_code']==200)
		    {
	        	this.feesLevels = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCenterFees(fees_level_id,name){
		let feesselectiontype = 'Class'
	    this.commonService.getCenterFees(fees_level_id,this.categoryId,this.courseId,feesselectiontype).subscribe((response)=>{
	    	if(response['status_code']==200)
		    {
		    	if(name == 'level'){
		    		this.Fees = response['body']
		    	}
		    	if(name == 'fees'){
			        this.fees_total_amount = response['body'][0]['fees_total_amount']
			        this.fees_id = response['body'][0]['fees_id']
			        this.fees_amount_collected = response['body'][0]['fees_total_amount']
			        this.fees_remaining_amount = '0'
			        this.maxinstallmentallowed = response['body'][0]['max_installment_allowed']
			    }
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      	
		        this.fees_total_amount = ''
		    }
	    })
	}

	onDobChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.dob = event.formatted
        }
        this.calcDate(this.programmestartdate,this.dob);
    }

    getCountries(){
	    this.commonService.getCountries().subscribe((response)=>{
	    	if(response['status_code']==200)
		    {
	        	this.countries = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getState(countryid){
		this.commonService.getState(countryid).subscribe((response)=>{
			if(response['status_code']==200)
		    {
	        	this.states = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCity(stateid){
		this.commonService.getCity(stateid).subscribe((response)=>{
			if(response['status_code']==200)
		    {
	        	this.cities = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getInquiryDetails(inquiryid){
		if(inquiryid != null){
			if(localStorage.getItem('studentid') == null){
				this.getEnquiry = inquiryid.value
				localStorage.setItem('enquiryNO',this.getEnquiry)
			}
			else{
				this.getEnquiry = inquiryid
			}
			this.commonService.getInquiryDetails(this.getEnquiry).subscribe((response)=>{
					this.isLoading = false
					if(response['status_code'] == 200){
						this.inquiryId = response['body'][0].inquiry_master_id;
				        this.academicyearid = response['body'][0].academic_year_master_id;
				        this.categoryId = response['body'][0].category_id;
						this.getCourses(this.categoryId);
						this.courseId = response['body'][0].course_id;
						this.getCenterUserBatches(this.categoryId,this.courseId)
						this.studentfirstname = response['body'][0].student_first_name;
						this.studentlastname = response['body'][0].student_last_name;
						this.dob=response['body'][0].student_dob;
						this.dob1=response['body'][0].student_dob.split('-');
						this.ngxstudentdob = {  
			                date: {  
			                    year: parseInt(this.dob1[2]),  
			                    month: parseInt(this.dob1[1]),  
			                    day: parseInt(this.dob1[0])
			                },
			            }
			            if(response['body'][0].has_attended_preschool_before == 'Yes'){
							this.attendedpreschoolbefore = true;
						}
						else{
							this.attendedpreschoolbefore = false;
						}
						this.preschoolname = response['body'][0].preschool_name;
						this.fatheremailid = response['body'][0].father_emailid;
						this.motheremailid = response['body'][0].mother_emailid;
						this.fathername = response['body'][0].father_name;
						this.mothername = response['body'][0].mother_name;
						this.fatherprof = response['body'][0].father_profession;
						this.motherprof = response['body'][0].mother_profession;
						this.presentaddress = response['body'][0].present_address;
						this.countryId = response['body'][0].country_id;
						this.getState(this.countryId);
						this.stateId = response['body'][0].state_id;
						this.getCity(this.stateId);
						this.cityId = response['body'][0].city_id;
						this.pincode = response['body'][0].pincode;
						this.fathermobilecontactno = response['body'][0].father_contact_no;
						this.mothermobilecontactno = response['body'][0].mother_contact_no;
						this.calcDate(this.programmestartdate,this.dob);
					}
					else{
					}
			    })
			// this.commonService.getInquiryId(this.getEnquiry).subscribe((response)=>{

			// 	let getInquiryId = response['body']['query_result'][0]['inquiry_master_id']
			// 	console.log(getInquiryId)

				
			// })
		}
	}

	feesPaymentTypeChange(feepaymenttype){
		if(this.feepaymenttype == true){

		}
		else{
			this.fees_amount_collected = this.fees_total_amount
	        this.fees_remaining_amount = '0'
		}
	}

	installmentnoChange(no){
		if(no > this.maxinstallmentallowed){
			this.snackbar.open('Installment No less then MaxInsatllment Allowed', '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});
			this.isLoading = false
            this.AdmissionTab5Form.get('noofinstallment').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
		}
		else{
			this.AdmissionTab5Form.get('noofinstallment').clearValidators();
        	this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
			if(no == null){
				this.installment = []
			}
			else{
				let num = no;
				for(var i = 0; i< no;i++){
					this.installment.push(i)
					if(this.total_amount == '' || this.total_amount == undefined){
						this.installment_amount = (this.fees_total_amount/num)
					}else{
						this.installment_amount = (this.total_amount/num)
					}
				}
			}
		}
	}

	discountChange(discount_amount){
		if(this.fees_total_amount == undefined){
			this.snackbar.open('Select Fees Level', '', {
				duration: 500,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
			this.isLoading = false
            this.AdmissionTab5Form.get('discount_amount').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('discount_amount').updateValueAndValidity();
		}
		else{
			this.AdmissionTab5Form.get('discount_amount').clearValidators();
        	this.AdmissionTab5Form.get('discount_amount').updateValueAndValidity();
        	this.total_amount = this.fees_total_amount - discount_amount
        	this.installment_amount = (this.total_amount/this.noofinstallment).toFixed(2);
		}
	}

	collectedinstallmentChange(index){
		let collected_amount = Number($('#id_'+index).val() )
		let amount = Number(this.installment_amount)
		if(Number(collected_amount) > Number(amount)){
			this.snackbar.open('Paying Amount grater than installment amount', '', {
				duration: 500,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
			this.isLoading = false
			$('#remaining_id_'+index).val(amount)
        }
        else{
			this.installment_remaining_amount = (amount - collected_amount)
			$('#remaining_id_'+index).val(this.installment_remaining_amount)
			this.AdmissionTab5Form.controls['installment_collected_amount'].setErrors(null);
			$('#amounterror_'+index).text('')
        }
	}

	monthChange(index){
		this.AdmissionTab5Form.controls['installment_due_date'].setErrors(null);
		$('#montherror_'+index).text('')	
	}

	initOptionsMap() {
	    for (var x = 0; x<this.payment_mode.length; x++) {
	        this.optionsMap[this.payment_mode[x]] = false;
	    }
	}

	updatePaymentOptions(option, event) {
   		this.optionsMap[option] = event.target.checked;
   		this.selectedOptions.push(option);
   		if(option == 'Cheque' && event.target.checked == true){
   			$('.cheque_no').show();
   		}
   		else{
   			$('.cheque_no').hide();
   		}
   		if(option == 'Netbanking' && event.target.checked == true){
   			$('.transaction_no').show();
   		}
   		else{
   			$('.transaction_no').hide();
   		}
   		if((option == 'Cheque' && event.target.checked == true) || (option == 'Netbanking' && event.target.checked == true)){
			$('.bank_name').show();
   		}
   		else{
   			$('.bank_name').hide();
   		}
	}

	updateOptions() {
	    for(var x in this.selectedOptions) {
	        if(this.selectedOptions[x]) {
	            this.optionsChecked.push(x);
	        }
	    }
	    this.payment_mode1 = this.optionsChecked;
	    this.optionsChecked = [];
	}

	sibling1enrollChnage(siblingenrollmentno){
		if(siblingenrollmentno.length >= 6){
			this.commonService.getStudentsWithEnrollmentNo(siblingenrollmentno).subscribe((response)=>{
	      		if(response['status_code']==200)
			    {
					this.sibling1name = response['body'][0]['student_first_name']+' '+response['body'][0]['student_last_name']
					this.sibling1id = response['body'][0]['student_id']
			    }
			    else
			    {
			    	this.snackbar.open(response['msg'], '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});  
			      
			    }
	      		
	    	})
		}
	}

	sibling2enrollChnage(siblingenrollmentno){
		if(siblingenrollmentno.length >= 6){
			this.commonService.getStudentsWithEnrollmentNo(siblingenrollmentno).subscribe((response)=>{
	      		if(response['status_code']==200)
			    {
					this.sibling2name = response['body'][0]['student_first_name']+' '+response['body'][0]['student_last_name']
					this.sibling2id = response['body'][0]['student_id']
			    }
			    else
			    {
			    	this.snackbar.open(response['msg'], '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});  
			      
			    }
	      		
	    	})
		}
	}

	admissionform1(admission){
		console.log(admission.index)
		if(admission.index == 0){
			this.Tab1(this.studentId)
			if(this.studentId != '' || this.studentId != null || this.studentId != undefined){
				this.Enquirydisable = false
				this.EnquirydisableCreate = true
			}

		}
		if(admission.index == 1){
			
			this.Tab2(this.studentId)
		}
		if(admission.index == 2){
			this.Tab3(this.studentId)
		}
		if(admission.index == 3){
			this.Tab4(this.studentId)
		}
		if(admission.index == 4){
			$('#admissionForm').text(this.duly_filled_admission_form)
			$('#birthcerti').text(this.birth_certificate)
			$('#profilePic').text(this.profile_pic)
			$('#familyphoto').text(this.family_photo)
			$('#addressProof').text(this.address_proof)
			$('#profileForm').text(this.duly_filled_child_profile_form)
			this.Tab5(this.studentId)
		}
		if(admission.index == 5){
			$('.cheque_no').hide();
			$('.transaction_no').hide();
			$('.bank_name').hide();
			// this.Tab5(this.studentId)
		}
		localStorage.setItem('tabindex',admission.index)
	}

	FormOnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionFormSubmitted = true;
		this.AdmissionTabForm.value.programmestartdate = this.programmestartdate
		this.AdmissionTabForm.value.programmeenddate = this.programmeenddate
		this.AdmissionTabForm.value.dob = this.dob
		let start = this.programmestartdate.split('-')
		let start_date = start[1]+'-'+start[0]+'-'+start[2]
		let end = this.programmeenddate.split('-')
		let end_date = end[1]+'-'+end[0]+'-'+end[2]
		if(new Date(start_date) > new Date(end_date)){
			this.isLoading = false
			this.snackbar.open('Program Start Date less than Program End Date', '', {
				duration: 500,
				verticalPosition:"top",
				panelClass: ['bg-red']
			}); 
			return
		}
		if (this.AdmissionTabForm.value.attendedpreschoolbefore === true) {
            this.isLoading = false
            this.AdmissionTabForm.get('preschoolname').setValidators([Validators.required]);

        	this.AdmissionTabForm.get('preschoolname').updateValueAndValidity();

        }
        else{
			this.AdmissionTabForm.get('preschoolname').clearValidators();
        	this.AdmissionTabForm.get('preschoolname').updateValueAndValidity();
        }
        this.AdmissionTabForm.value.inquiryid = this.inquiryId
		if(this.AdmissionTabForm.invalid){
			this.isLoading = false
			return;
    	}
    	this.commonService.addEditAdmissionTab1(this.AdmissionTabForm.value,this.studentId).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
		    	this.admissionForm1disable = false
				this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				this.admissionForm1disable = false
				this.selectedindex = 1
				this.admissionFilter.enrollmentno = response.Data;
				this.commonService.listAdmissions((this.page) ,this.size,this.admissionFilter).subscribe((response) => {
		            if(response['status_code'] == 200){
		            	this.studentId = response['body']['query_result'][0].student_id
		            }
					if(this.route.snapshot.params.studentid)
					{
        			}
        			else{
        				localStorage.setItem('studentid',this.studentId)
        			}
		        },
		        (err: any) => console.log("error",err),() =>
		            console.log()
		        );
		    }
		    else
		    {
		    	this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

	Form1OnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionForm1Submitted = true;
		// if(this.sibling1dob != undefined){
		// 	this.AdmissionTab1Form.value.sibling1dob = this.sibling1dob
		// }
		// else{
		// 	this.AdmissionTab1Form.value.sibling1dob = '00-00-0000'	
		// }
		// if(this.sibling2dob != undefined){
		// 	this.AdmissionTab1Form.value.sibling2dob = this.sibling2dob
		// }
		// else{
		// 	this.AdmissionTab1Form.value.sibling2dob = '00-00-0000'
		// }

		// if (this.AdmissionTab1Form.value.sibling1name != '') {
  //           console.log(1)
  //           this.AdmissionTab1Form.get('sibling1dob').setValidators([Validators.required]);

  //       	this.AdmissionTab1Form.get('sibling1dob').updateValueAndValidity();
  //           this.AdmissionTab1Form.get('sibling1school').setValidators([Validators.required]);

  //       	this.AdmissionTab1Form.get('sibling1school').updateValueAndValidity();

  //       }
  //       else{
  //       	console.log(2)
		// 	this.AdmissionTab1Form.get('sibling1dob').clearValidators();
  //       	this.AdmissionTab1Form.get('sibling1dob').updateValueAndValidity();

		// 	this.AdmissionTab1Form.get('sibling1school').clearValidators();
  //       	this.AdmissionTab1Form.get('sibling1school').updateValueAndValidity();
  //       }
		// if (this.AdmissionTab1Form.value.sibling2name != '') {
  //           console.log(3)
  //           this.AdmissionTab1Form.get('sibling2dob').setValidators([Validators.required]);

  //       	this.AdmissionTab1Form.get('sibling2dob').updateValueAndValidity();
  //           this.AdmissionTab1Form.get('sibling2school').setValidators([Validators.required]);

  //       	this.AdmissionTab1Form.get('sibling2school').updateValueAndValidity();

  //       }
  //       else{
  //       	console.log(4);
		// 	this.AdmissionTab1Form.get('sibling2dob').clearValidators();
  //       	this.AdmissionTab1Form.get('sibling2dob').updateValueAndValidity();
        	
		// 	this.AdmissionTab1Form.get('sibling2school').clearValidators();
  //       	this.AdmissionTab1Form.get('sibling2school').updateValueAndValidity();
  //       }
		if(this.AdmissionTab1Form.invalid){
			this.isLoading = false
			return;
    	}
    	this.commonService.admissionTab2(this.AdmissionTab1Form.value,this.studentId).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
		    	this.admissionForm2disable = false
				this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				this.admissionForm2disable = false
				this.selectedindex = 2
		    }
		    else
		    {
		    	this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

	Form2OnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionForm2Submitted = true;
		if(this.AdmissionTab2Form.invalid){
			this.isLoading = false
		    return;
    	}
    	this.commonService.admissionTab3(this.AdmissionTab2Form.value,this.studentId).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
		    	this.admissionForm3disable = false
				this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				this.admissionForm3disable = false
				this.selectedindex = 3
		    }
		    else
		    {
		    	this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

	Form3OnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionForm3Submitted = true;
		if(this.AdmissionTab3Form.invalid){
			this.isLoading = false
		    return;
    	}
    	this.commonService.admissionTab4(this.AdmissionTab3Form.value,this.studentId).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
		    	this.admissionForm4disable = false
				this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				this.admissionForm4disable = false
				this.selectedindex = 4
		    }
		    else
		    {
		    	this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

	Form4OnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionForm4Submitted = true;
		if(this.admissionFormSelFile == ''){
			this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
		}
		else{
			this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
		}
		if(this.birthCerySelFile == ''){
			this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
		}
		else{
			this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
		}
		if(this.profilePicSelFile == ''){
			this.AdmissionTab4Form.value.profile_pic = this.profile_pic
		}
		else{
			this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
		}
		if(this.familyPhotoSelFile == ''){
			this.AdmissionTab4Form.value.family_photo = this.family_photo
		}
		else{
			this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
		}
		if(this.addressProofSelFile == ''){
			this.AdmissionTab4Form.value.address_proof = this.address_proof
		}
		else{
			this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
		}
		if(this.profileFormSelFile == ''){
			this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
		}
		else{
			this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
		} 
		if(this.AdmissionTab4Form.invalid && this.AdmissionTab4Form.value.duly_filled_admission_form == '' && this.AdmissionTab4Form.value.birth_certificate == '' && this.AdmissionTab4Form.value.profile_pic == '' && this.AdmissionTab4Form.value.family_photo == '' && this.AdmissionTab4Form.value.address_proof == '' && this.AdmissionTab4Form.value.duly_filled_child_profile_form == ''){
		    this.isLoading = false
		    return;
    	}
    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
		    	this.admissionForm5disable = false
		    	localStorage.setItem('docsave','dacSave')
				this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				if(this.route.snapshot.params.studentid)
				{
					this.router.navigate(['/admissionlist']);
				}
				else{
					this.admissionForm5disable = false
					this.selectedindex = 5
				}
		    }
		    else
		    {
		    	this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

	Form5OnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionForm5Submitted = true;
		let collected_amount = []
		let remainig_amount = []
		let monthArray = []
		let itemsArray = []
		let installmentNo = []
		if(formValue.noofinstallment == ''){
		}
		else{
			for(var i=0 ;i < this.noofinstallment ;i++){
				this.isLoading = false
				let amount = $('#id_'+i).val()
				if(amount == ''){ 
					this.AdmissionTab5Form.controls['installment_collected_amount'].setErrors({'incorrect': true});
					$('#amounterror_'+i).text('Enter amount')
				}
				else{
					this.AdmissionTab5Form.controls['installment_collected_amount'].setErrors(null);
					$('#amounterror_'+i).text('')	
				}
				console.log(this.installment_amount)
				if(Number(amount) > Number(this.installment_amount)){
					this.snackbar.open('Paying Amount grater than installment amount', '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});
					return
		        }
				let remainig_amnt = $('#remaining_id_'+i).val();
				let month_no = $('#month_id_'+i).val();
				if(month_no == null){ 
					this.AdmissionTab5Form.controls['installment_due_date'].setErrors({'incorrect': true});
					$('#montherror_'+i).text('Select Month') 
				}
				else{
					this.AdmissionTab5Form.controls['installment_due_date'].setErrors(null);
					$('#montherror_'+i).text('')	
				}
				collected_amount.push(amount)
				remainig_amount.push(remainig_amnt)
				monthArray.push(month_no)
			}
			installmentNo = [this.maxinstallmentallowed,this.noofinstallment]
			if(this.AdmissionTab5Form.value.noofinstallment > this.AdmissionTab5Form.value.maxinstallmentallowed){
	        	this.snackbar.open('Installment No less then MaxInsatllment Allowed', '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				}); 
				return
	        } 
		}
		if (this.AdmissionTab5Form.value.feepaymenttype === true) {
			this.isLoading = false
            this.AdmissionTab5Form.get('noofinstallment').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();

        }
        else{
			this.AdmissionTab5Form.get('noofinstallment').clearValidators();
        	this.AdmissionTab5Form.get('noofinstallment').updateValueAndValidity();
        }
        if (this.AdmissionTab5Form.value.payment_mode === 'Cheque') {
        	this.isLoading = false
            this.AdmissionTab5Form.get('cheque_no').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('cheque_no').updateValueAndValidity();

        }
        else{
			this.AdmissionTab5Form.get('cheque_no').clearValidators();
        	this.AdmissionTab5Form.get('cheque_no').updateValueAndValidity();
        }
        if (this.AdmissionTab5Form.value.payment_mode === 'Netbanking') {
            this.isLoading = false
            this.AdmissionTab5Form.get('transaction_id').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('transaction_id').updateValueAndValidity();
            this.AdmissionTab5Form.get('transaction_date').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('transaction_date').updateValueAndValidity();

        }
        else{
			this.AdmissionTab5Form.get('transaction_id').clearValidators();
        	this.AdmissionTab5Form.get('transaction_id').updateValueAndValidity();
			this.AdmissionTab5Form.get('transaction_date').clearValidators();
        	this.AdmissionTab5Form.get('transaction_date').updateValueAndValidity();
        }

        if (this.AdmissionTab5Form.value.payment_mode === 'Cheque' || this.AdmissionTab5Form.value.payment_mode === 'Netbanking') {
            this.AdmissionTab5Form.get('bank_name').setValidators([Validators.required]);

        	this.AdmissionTab5Form.get('bank_name').updateValueAndValidity();
        }
        else{
			this.AdmissionTab5Form.get('bank_name').clearValidators();
        	this.AdmissionTab5Form.get('bank_name').updateValueAndValidity();

        }

		this.AdmissionTab5Form.value.categoryid = this.categoryId
		this.AdmissionTab5Form.value.courseid = this.courseId
		this.AdmissionTab5Form.value.batchid = this.batchId
		if (this.AdmissionTab5Form.value.payment_mode === '') {
            this.isLoading = false
            this.AdmissionTab5Form.value.payment_mode = 'Cash'

        }
		if(this.AdmissionTab5Form.invalid){
			this.isLoading = false
		    return;
    	}
    	this.commonService.payAdmissionFees(this.AdmissionTab5Form.value,this.studentId,collected_amount,remainig_amount,monthArray,installmentNo).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
		    	localStorage.removeItem('studentid')
		    	localStorage.removeItem('tabindex')
		    	localStorage.removeItem('enquiryNO')
		    	localStorage.removeItem('docsave')
				localStorage.removeItem('category')
				localStorage.removeItem('course')
				this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				window.open(response['body'], '_blank');
				this.router.navigate(['/admissionlist']);
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
    	
	}

	onadmissionFormUpload(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png") ||(event.target.files[0].type=="application/pdf"))
		{
			document.getElementById("admissionForm").innerHTML = event.target.files[0].name
			this.admissionFormSelFile = event.target.files[0]
			this.isLoading = true
			if(localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined){
				if(this.admissionFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
				}
				if(this.birthCerySelFile == ''){
					this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
				}
				else{
					this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
				}
				if(this.profilePicSelFile == ''){
					this.AdmissionTab4Form.value.profile_pic = this.profile_pic
				}
				else{
					this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
				}
				if(this.familyPhotoSelFile == ''){
					this.AdmissionTab4Form.value.family_photo = this.family_photo
				}
				else{
					this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
				}
				if(this.addressProofSelFile == ''){
					this.AdmissionTab4Form.value.address_proof = this.address_proof
				}
				else{
					this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
				}
				if(this.profileFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
				}
		    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
		      		if(response['status_code']==200)
				    {
				    	if(this.route.snapshot.params.studentid){
				    		this.studentidpass = this.route.snapshot.params.studentid
				    	}
				    	else{
				    		this.studentidpass = localStorage.getItem('studentid')
				    	}
						this.commonService.getAdmissionDocs(this.studentidpass)
						.subscribe(
				 			data => {
				 			this.isLoading = false
				 			if(data['status_code'] == 200){
					 			this.admission_form_path = data['body'][0].duly_filled_admission_form_path;
							}
						})
				    }
		    	})
			}
		}
		else{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
	}

	onbirthCertyUpload(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png") ||(event.target.files[0].type=="application/pdf"))
		{
			document.getElementById("birthcerti").innerHTML = event.target.files[0].name
			this.birthCerySelFile = event.target.files[0]
			this.isLoading = true
			if(localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined){
				if(this.admissionFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
				}
				if(this.birthCerySelFile == ''){
					this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
				}
				else{
					this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
				}
				if(this.profilePicSelFile == ''){
					this.AdmissionTab4Form.value.profile_pic = this.profile_pic
				}
				else{
					this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
				}
				if(this.familyPhotoSelFile == ''){
					this.AdmissionTab4Form.value.family_photo = this.family_photo
				}
				else{
					this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
				}
				if(this.addressProofSelFile == ''){
					this.AdmissionTab4Form.value.address_proof = this.address_proof
				}
				else{
					this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
				}
				if(this.profileFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
				}
		    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
		      		if(response['status_code']==200)
				    {
				    	if(this.route.snapshot.params.studentid){
				    		this.studentidpass = this.route.snapshot.params.studentid
				    	}
				    	else{
				    		this.studentidpass = localStorage.getItem('studentid')
				    	}
						this.commonService.getAdmissionDocs(this.studentidpass)
						.subscribe(
				 			data => {
				 			this.isLoading = false
				 			if(data['status_code'] == 200){
					 			this.birth_certificate_path = data['body'][0].birth_certificate_path;
							}
						})
				    }
		    	})
		    }
		}
		else{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
	}

	onprofilePicUpload(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png"))
		{
			document.getElementById("profilePic").innerHTML = event.target.files[0].name
			this.profilePicSelFile = event.target.files[0]
			this.isLoading = true
			if(localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined){
				if(this.admissionFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
				}
				if(this.birthCerySelFile == ''){
					this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
				}
				else{
					this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
				}
				if(this.profilePicSelFile == ''){
					this.AdmissionTab4Form.value.profile_pic = this.profile_pic
				}
				else{
					this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
				}
				if(this.familyPhotoSelFile == ''){
					this.AdmissionTab4Form.value.family_photo = this.family_photo
				}
				else{
					this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
				}
				if(this.addressProofSelFile == ''){
					this.AdmissionTab4Form.value.address_proof = this.address_proof
				}
				else{
					this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
				}
				if(this.profileFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
				}
		    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
		      		if(response['status_code']==200)
				    {
				    	if(this.route.snapshot.params.studentid){
				    		this.studentidpass = this.route.snapshot.params.studentid
				    	}
				    	else{
				    		this.studentidpass = localStorage.getItem('studentid')
				    	}
				    	this.commonService.getAdmissionDocs(this.studentidpass)
						.subscribe(
				 			data => {
				 			this.isLoading = false
				 			if(data['status_code'] == 200){
					 			this.profile_pic_path = data['body'][0].profile_pic_path;
							}
						})
				    }
		      		
		    	})
		    }
		}
		else{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
	}

	onfamilyPhotoUpload(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png"))
		{
			document.getElementById("familyphoto").innerHTML = event.target.files[0].name
			this.familyPhotoSelFile = event.target.files[0]

			this.isLoading = true
			if(localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined){
				if(this.admissionFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
				}
				if(this.birthCerySelFile == ''){
					this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
				}
				else{
					this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
				}
				if(this.profilePicSelFile == ''){
					this.AdmissionTab4Form.value.profile_pic = this.profile_pic
				}
				else{
					this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
				}
				if(this.addressProofSelFile == ''){
					this.AdmissionTab4Form.value.address_proof = this.address_proof
				}
				else{
					this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
				}
				if(this.profileFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
				}

				if(this.familyPhotoSelFile == ''){
					this.AdmissionTab4Form.value.family_photo = this.family_photo
				}
				else{
					this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
				}
		    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
		      		if(response['status_code']==200)
				    {
				    	if(this.route.snapshot.params.studentid){
				    		this.studentidpass = this.route.snapshot.params.studentid
				    	}
				    	else{
				    		this.studentidpass = localStorage.getItem('studentid')
				    	}
				    	this.commonService.getAdmissionDocs(this.studentidpass)
						.subscribe(
				 			data => {
				 			this.isLoading = false
				 			if(data['status_code'] == 200){
					 			this.family_photo_path = data['body'][0].family_photo_path;
							}
						})
				    }
		      		
		    	})
		    }
		}
		else{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
	}

	onaddressProofUpload(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png") ||(event.target.files[0].type=="application/pdf"))
		{
			document.getElementById("addressProof").innerHTML = event.target.files[0].name
			this.addressProofSelFile = event.target.files[0]
			this.isLoading = true
			if(localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined){
				if(this.admissionFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
				}
				if(this.birthCerySelFile == ''){
					this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
				}
				else{
					this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
				}
				if(this.profilePicSelFile == ''){
					this.AdmissionTab4Form.value.profile_pic = this.profile_pic
				}
				else{
					this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
				}
				if(this.familyPhotoSelFile == ''){
					this.AdmissionTab4Form.value.family_photo = this.family_photo
				}
				else{
					this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
				}
				if(this.addressProofSelFile == ''){
					this.AdmissionTab4Form.value.address_proof = this.address_proof
				}
				else{
					this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
				}
				if(this.profileFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
				}
		    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
		      		if(response['status_code']==200)
				    {
				    	if(this.route.snapshot.params.studentid){
				    		this.studentidpass = this.route.snapshot.params.studentid
				    	}
				    	else{
				    		this.studentidpass = localStorage.getItem('studentid')
				    	}
						this.commonService.getAdmissionDocs(this.studentidpass)
						.subscribe(
				 			data => {
				 			this.isLoading = false
				 			if(data['status_code'] == 200){
					 			this.address_proof_path = data['body'][0].address_proof_path;
							}
						})
					}
		    	})
		    }
		}
		else{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
	}

	onprofileFormUpload(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png") ||(event.target.files[0].type=="application/pdf"))
		{
			document.getElementById("profileForm").innerHTML = event.target.files[0].name
			this.profileFormSelFile = event.target.files[0]
			this.isLoading = true
			if(localStorage.getItem('docsave') != null || localStorage.getItem('docsave') != '' || localStorage.getItem('docsave') != undefined){
				if(this.admissionFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.duly_filled_admission_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_admission_form = this.admissionFormSelFile
				}
				if(this.birthCerySelFile == ''){
					this.AdmissionTab4Form.value.birth_certificate = this.birth_certificate
				}
				else{
					this.AdmissionTab4Form.value.birth_certificate = this.birthCerySelFile
				}
				if(this.profilePicSelFile == ''){
					this.AdmissionTab4Form.value.profile_pic = this.profile_pic
				}
				else{
					this.AdmissionTab4Form.value.profile_pic = this.profilePicSelFile
				}
				if(this.familyPhotoSelFile == ''){
					this.AdmissionTab4Form.value.family_photo = this.family_photo
				}
				else{
					this.AdmissionTab4Form.value.family_photo = this.familyPhotoSelFile
				}
				if(this.addressProofSelFile == ''){
					this.AdmissionTab4Form.value.address_proof = this.address_proof
				}
				else{
					this.AdmissionTab4Form.value.address_proof = this.addressProofSelFile
				}
				if(this.profileFormSelFile == ''){
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.duly_filled_child_profile_form
				}
				else{
					this.AdmissionTab4Form.value.duly_filled_child_profile_form = this.profileFormSelFile
				}
		    	this.commonService.addEditAdmissionDocs(this.AdmissionTab4Form.value,this.studentId).subscribe((response)=>{
		    		this.isLoading = false
		      		if(response['status_code']==200)
				    {
				    	if(this.route.snapshot.params.studentid){
				    		this.studentidpass = this.route.snapshot.params.studentid
				    	}
				    	else{
				    		this.studentidpass = localStorage.getItem('studentid')
				    	}
						this.commonService.getAdmissionDocs(this.studentidpass)
						.subscribe(
				 			data => {
				 			if(data['status_code'] == 200){
					 			this.profile_form_path = data['body'][0].duly_filled_child_profile_form_path;
							}
						})
				    }
		      		
		    	})
		    }
		}
		else{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
	}

}

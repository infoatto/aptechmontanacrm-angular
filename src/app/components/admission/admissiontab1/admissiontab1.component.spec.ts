import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Admissiontab1Component } from './admissiontab1.component';

describe('Admissiontab1Component', () => {
  let component: Admissiontab1Component;
  let fixture: ComponentFixture<Admissiontab1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Admissiontab1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Admissiontab1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemainingfeesComponent } from './remainingfees.component';

describe('RemainingfeesComponent', () => {
  let component: RemainingfeesComponent;
  let fixture: ComponentFixture<RemainingfeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemainingfeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemainingfeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

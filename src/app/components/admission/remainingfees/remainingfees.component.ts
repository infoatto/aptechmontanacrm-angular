import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit , NgZone} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';
import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: 'app-remainingfees',
  templateUrl: './remainingfees.component.html',
  styleUrls: ['./remainingfees.component.css']
})
export class RemainingfeesComponent implements OnInit {


    feestype :boolean = false
	public AdmissionRemainigPayFees: FormGroup;
	AdmissionFormSubmitted = false;
	categoryId = ""
	courseId = ""
	batchId = ""
	category : any
	courses : any
	batchid = ""
	batches : any
    isLoading = false
	public students: Array<Select2OptionData>;
    optionsSelect: Select2Options;
    studentId = ""
    admissionfeesid = ''
	fees_total_amount : any
	fees_amount_collected = ""
	fees_remaining_amount = ""
	payFees = false
    feepaymenttype :boolean = false
    installment = []
    installment_amount : any
    installment_remaining_amount = 0
    key :number = 0
    installmentArray = []
    fees_remark = ""
    fees_approved_accepted_by_name = ""
	ngpayment_mode : any
    payment_mode = ['Cheque', 'Cash', 'Netbanking'];
   	payment_mode1 : any
   	selectedOptions = [];
	optionsMap = {
        Cheque: false,
        Cash: false,
        Netbanking: false,
	};
	optionsChecked = [];
	installmentNo : any
    bank_name = ""
    transaction_date = ''
	transaction_date1 : any
	ngxtransactiondate : any  = {  
	    jsdate: new Date()  
	}; 

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
		private snackbar:MatSnackBar,
		private router: Router,
        private zone: NgZone,
	) { }

	ngOnInit() {
		localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
		localStorage.removeItem('category')
		localStorage.removeItem('course')
		this.optionsSelect = {
			placeholder: "Select Student",
			allowClear: true,
			width: "100%",
		}

		$('.cheque_no').hide();
		$('.transaction_no').hide();
		$('.bank_name').hide();
		this.ngpayment_mode = 'Cash'
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.fees_approved_accepted_by_name =  currentUser.first_name + ' ' + currentUser.last_name

		this.AdmissionRemainigPayFees = this.formBuilder.group({
			feestype : [''],
			categoryid : ['',[Validators.required]],
			courseid : ['',[Validators.required]],
			batchid : ['',[Validators.required]],
			fees_total_amount : [''],
			fees_amount_collected : [''],
			fees_remaining_amount : [''],
			feepaymenttype : [''],
			installment_collected_amount : [''],
			installment_remaining_amount : [''],
			fees_remark : ['',[Validators.required]],
			fees_approved_accepted_by_name : ['',[Validators.required]],
			payment_mode : ['',[Validators.required]],
			cheque_no : [''],
			transaction_id : [''],
			noofinstallment : [''],
			admission_fees_instalment_id : [''],
			bank_name : [''],
			transaction_date : [''],
		})
		this.getCategories();
	}

	getCategories(){
	    this.commonService.getCategories().subscribe((response)=>{
	        if(response['status_code']==200)
		    {
	        	this.category = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCourses(categoryid){
	    this.commonService.getCourses(categoryid).subscribe((response)=>{
	    	if(response['status_code']==200)
		    {
	        	this.courses = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCenterUserBatches(categoryid,courseid){
		this.commonService.getCenterUserBatches(categoryid,courseid).subscribe((response)=>{
			if(response['status_code']==200)
		    {
	        	this.batches = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getStudent(categoryid,courseid,batchid){
		if(categoryid != '' && courseid != '' && batchid != ''){
			this.commonService.getStudentList(categoryid,courseid,batchid,this.feestype).subscribe((response)=>{
				if(response['status_code']==200)
			    {
			    	// var arr = [];
			    	// for(var i=0; i<response['body'].length; i++) {
		      //           var no = response['body'][i].enrollment_no ;

		      //           var first_name = response['body'][i].student_first_name ;
		      //           var last_name = response['body'][i].student_last_name ;
		      //           arr.push(no + ' (' + first_name + ' ' + last_name +' )');
		      //       }
					this.students = response['body']
			    }
			    else
			    {
			    	this.snackbar.open(response['msg'], '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});  
			      
			    }
		    })
		}
	}

	collectedinstallmentChange(index){
		let collected_amount = $('#collected_amount_'+index).val()
		let paying_amount = Number($('#id_'+index).val() )
		let amount = Number(this.installment_amount)
		$('#id_'+index).val(paying_amount)
		let remaining_amount = $('#remaining_id_'+index).val()
		console.log(paying_amount)
		// if(Number(paying_amount) > Number(remaining_amount)){
		// 			}
		// // if(paying_amount == 0 || paying_amount == 0.00){
		// // 	this.snackbar.open('Paying Amount could not be 0', '', {
		// // 		duration: 500,
		// // 		verticalPosition:"top",
		// // 		panelClass: ['bg-red']
		// // 	});
		// // 	this.isLoading = false
		// // 	$('#remaining_id_'+index).val(remaining_amount)
		// // }
		// else{
			// if(collected_amount != 0.00){
				let total_minus_amount = Number(collected_amount) + Number(paying_amount)
				if(Number(total_minus_amount) > Number(collected_amount)){
					this.snackbar.open('Paying Amount grater than remianing amount', '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});
					this.isLoading = false
					console.log(collected_amount)
					// $('#remaining_id_'+index).val(collected_amount)

				}
				else{
				console.log(total_minus_amount)
					this.installment_remaining_amount = (amount - total_minus_amount)
				// }
				// else{
				// 	this.installment_remaining_amount = (amount - paying_amount)
				// }
				$('#remaining_id_'+index).val(this.installment_remaining_amount.toFixed(2))
				this.AdmissionRemainigPayFees.controls['installment_collected_amount'].setErrors(null);
				$('#amounterror_'+index).text('')

				}
		// }
	}

	getStudentDetails(enrollmentno){
		let getStudentId = enrollmentno.value
		this.commonService.getStudentPaymentDetails(getStudentId,this.feestype,this.categoryId,this.courseId).subscribe((response)=>{
					if(response['status_code'] == 200){
						console.log(response)
						this.payFees = true
						this.admissionfeesid = response['body'][0]['admission_fees_id']
						this.studentId = response['body'][0]['student_id']
						this.fees_total_amount = response['body'][0]['fees_total_amount']
						this.fees_amount_collected = response['body'][0]['fees_amount_collected']
						this.fees_remaining_amount = response['body'][0]['fees_remaining_amount']
						this.installmentNo = response['body'][0]['no_of_installments']
						if(response['body'][0]['is_instalment'] == 'Yes'){
							this.feepaymenttype = true
							this.installment_amount = response['body'][0]['instalment_details'][0]['instalment_amount']
						}
						else{
							this.feepaymenttype = false
						}
						this.installment = response['body'][0]['instalment_details']	
					}
					else{
						this.payFees = false
						this.snackbar.open(response['msg'], '', {
							duration: 500,
							verticalPosition:"top",
							panelClass: ['bg-red']
						}); 
					}
			    })
			// this.commonService.getStudentId(enrollmentsplit[0]).subscribe((response)=>{

			// 	let getStudentId = response['body']['query_result'][0]['student_id']
			// 	console.log(getStudentId)
				
			// })
	}

	initOptionsMap() {
	    for (var x = 0; x<this.payment_mode.length; x++) {
	        this.optionsMap[this.payment_mode[x]] = false;
	    }
	}

	updatePaymentOptions(option, event) {
   		this.optionsMap[option] = event.target.checked;
   		this.selectedOptions.push(option);
   		if(option == 'Cheque' && event.target.checked == true){
   			$('.cheque_no').show();
   		}
   		else{
   			$('.cheque_no').hide();
   		}
   		if(option == 'Netbanking' && event.target.checked == true){
   			$('.transaction_no').show();
   		}
   		else{
   			$('.transaction_no').hide();
   		}
   		if((option == 'Cheque' && event.target.checked == true) || (option == 'Netbanking' && event.target.checked == true)){
			$('.bank_name').show();
   		}
   		else{
   			$('.bank_name').hide();
   		}
	}

	updateOptions() {
	    for(var x in this.selectedOptions) {
	        if(this.selectedOptions[x]) {
	            this.optionsChecked.push(x);
	        }
	    }
	    this.payment_mode1 = this.optionsChecked;
	    this.optionsChecked = [];
	}

	ontransactionDateChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.transaction_date = event.formatted
        }
    }

	OnSubmit(formValue : any){
		this.isLoading = true
		this.AdmissionFormSubmitted = true;

		this.AdmissionRemainigPayFees.value.categoryid = this.categoryId
		this.AdmissionRemainigPayFees.value.courseid = this.courseId
		this.AdmissionRemainigPayFees.value.batchid = this.batchId
		let collected_amount = []
		let remainig_amount = []
		let installment_amount = []
		console.log(this.AdmissionRemainigPayFees.value)
		if(this.feepaymenttype == false){
		}
		else{
			this.AdmissionRemainigPayFees.value.feepaymenttype = 'Yes'
			for(var i=0 ;i < this.installmentNo ;i++){
				this.isLoading = false
				let amount = $('#id_'+i).val()
				let installmentamnt = $('#admission_fees_instalment_id_'+i).val()
				console.log(amount)
				if(amount == ''){ 
					this.AdmissionRemainigPayFees.controls['installment_collected_amount'].setErrors({'incorrect': true});
					$('#amounterror_'+i).text('Enter amount')
				}
				else{
					this.AdmissionRemainigPayFees.controls['installment_collected_amount'].setErrors(null);
					$('#amounterror_'+i).text('')	
				}
				let remainig_amnt = $('#remaining_id_'+i).val();
				if(Number(amount) > Number(remainig_amnt)){
					this.snackbar.open('Paying Amount grater than remianing amount', '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});
					this.isLoading = false
					return
				}
				// if(amount == 0 || amount == 0.00){
				// 	this.snackbar.open('Paying Amount could not be 0', '', {
				// 		duration: 500,
				// 		verticalPosition:"top",
				// 		panelClass: ['bg-red']
				// 	});
				// 	this.isLoading = false
				// }
				if(amount != undefined){
					collected_amount.push(amount)
					remainig_amount.push(remainig_amnt)
					installment_amount.push(installmentamnt)
				}
			}		
		}
        if (this.AdmissionRemainigPayFees.value.payment_mode === 'Cheque') {
        	this.isLoading = false
            this.AdmissionRemainigPayFees.get('cheque_no').setValidators([Validators.required]);

        	this.AdmissionRemainigPayFees.get('cheque_no').updateValueAndValidity();

        }
        else{
			this.AdmissionRemainigPayFees.get('cheque_no').clearValidators();
        	this.AdmissionRemainigPayFees.get('cheque_no').updateValueAndValidity();
        }
        if (this.AdmissionRemainigPayFees.value.payment_mode === 'Netbanking') {
            this.isLoading = false
            this.AdmissionRemainigPayFees.get('transaction_id').setValidators([Validators.required]);

        	this.AdmissionRemainigPayFees.get('transaction_id').updateValueAndValidity();
        	 this.AdmissionRemainigPayFees.get('transaction_date').setValidators([Validators.required]);

        	this.AdmissionRemainigPayFees.get('transaction_date').updateValueAndValidity();
        }
        else{
			this.AdmissionRemainigPayFees.get('transaction_id').clearValidators();
        	this.AdmissionRemainigPayFees.get('transaction_id').updateValueAndValidity();
        }
        if (this.AdmissionRemainigPayFees.value.payment_mode === 'Cheque' || this.AdmissionRemainigPayFees.value.payment_mode === 'Netbanking') {
            this.AdmissionRemainigPayFees.get('bank_name').setValidators([Validators.required]);

        	this.AdmissionRemainigPayFees.get('bank_name').updateValueAndValidity();
        }
        else{
			this.AdmissionRemainigPayFees.get('bank_name').clearValidators();
        	this.AdmissionRemainigPayFees.get('bank_name').updateValueAndValidity();
        	this.AdmissionRemainigPayFees.get('transaction_date').clearValidators();
        	this.AdmissionRemainigPayFees.get('transaction_date').updateValueAndValidity();
        }
		if(this.AdmissionRemainigPayFees.invalid){
			this.isLoading = false
			return;
    	}
    	this.commonService.payAdmissionRemainingFees(this.AdmissionRemainigPayFees.value,this.studentId,this.admissionfeesid,this.installmentNo,collected_amount,remainig_amount,installment_amount).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
				this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				window.open(response['body'], '_blank');
				this.router.navigate(['/']);
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

}

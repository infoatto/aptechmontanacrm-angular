import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-constant',
  templateUrl: './constant.component.html',
  styleUrls: ['./constant.component.css']
})
export class ConstantComponent{
	
  public static defaultLogo= 'assets/images/aptech-montana-logo.jpg'

}

import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Select2OptionData } from 'ng2-select2';

@Component({
	selector: 'app-addeditenquiry',
	templateUrl: './addeditenquiry.component.html',
	styleUrls: ['./addeditenquiry.component.css']
})
export class AddeditenquiryComponent implements OnInit {

	myOptions: INgxMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
    }
    myOptionsInquirydate : INgxMyDpOptions = {
        dateFormat: 'dd-mm-yyyy', 
    }

    disableYear : boolean = false
    disableEnquirydate : boolean = false
   	how_know_about_school = ['Website', 'Newspaper', 'FacebookPage','JustDial','SchoolSignBoard','Reference','PosterBanner','Leaflet','Other'];
   	how_know_about_school1 : any
   	selectedOptions = [];
	optionsMap = {
        Website: true,
        Newspaper: false,
        FacebookPage: false,
        JustDial: false,
        SchoolSignBoard: false,
        Reference: false,
        PosterBanner: false,
        Leaflet: false,
        Other: false,
	};
	inquiryDate : boolean = false
	optionsChecked = [];

	public EnquiryForm: FormGroup;
	inquirydate : any;
	inquirydate1 : any;
	ngxinquirydate : any = {  
	    jsdate: new Date()  
	};  
	student_dob : any;
	student_dob1 : any;
	ngxstudentdob : any;
	inquiryno = "";
	student_first_name = "";
	student_last_name = "";
	age_year: any;
	age_month : any;
	father_name = "";
	mother_name = "";
	present_address = "";
	pincode = "";
	father_contact_no = '';
	mother_contact_no = "";
	father_emailid = '';
	mother_emailid = '';
	father_education = '';
	mother_education = '';
	father_profession = '';
	mother_profession = '';
	inquiry_remark = '';
	inquiry_handled_by = ''; 
	enquiryFormSubmitted = false;
	category : any
	courses : any
	countries : any
	states : any
	cities : any
  	enquiryId:any = "";
    academicyears : any
    categoryId : any
    courseId : any
    countryId : any
    stateId : any
    cityId : any
    academicyear : any
    gender : any
    rectype : boolean = false
    preschool_name : any
    has_attended_preschool_before : any
    selectedCheckbox : any
    how_know_about_other = ""
    nghow_know_about_school : any
    isLoading = false
    public leads: Array<Select2OptionData>;
    optionsSelect: Select2Options;
    inquiryId : any
    getEnquiry : any
    statusId = ""
    progressId = ""
    leadId : any
    stauts = ['Hot','Cold','Warm']
    progress = ['Meeting','Call','Walk inn']
    enquirydisable : boolean = false
    leaddisable : boolean = false
    leadenableCreate : boolean = true
    updated_on : any
    leaddisableCreate : boolean = false
    lead1 : any
    remark_date = ''

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
    	private snackbar:MatSnackBar,
    	private router: Router,
	) { }

	ngOnInit() {
		this.optionsSelect = {
			placeholder: "Select Enquiry",
			allowClear: true,
			width: "100%",
		}
		this.academicyear = '2'
		this.nghow_know_about_school = 'Website'
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.inquiry_handled_by =  currentUser.first_name + ' ' + currentUser.last_name
		$('.other').hide();
		this.EnquiryForm = this.formBuilder.group({
			inquiryno : [''],
			inquirydate : [''],
			student_first_name : ['',[Validators.required]],
			student_last_name : ['',[Validators.required]],
			student_dob : [''],
			age_year : [''],
			age_month : [''],
			category_id : [''],
			course_id : [''],
			has_attended_preschool_before : [''],
			preschool_name : [''],
			gender : [''],
			rectype : [''],
			academicyear : [''],
			father_name : ['',[Validators.required]],
			mother_name : [''],
			present_address : [''],
			country_id : ['',[Validators.required]],
			state_id : ['',[Validators.required]],
			city_id : ['',[Validators.required]],
			pincode : ['',[Validators.required]],
			father_contact_no : ['',[Validators.required]],
			mother_contact_no : [''],
			father_emailid : ['', [Validators.email]],
			mother_emailid : [''],
			father_education : [''],
			mother_education : [''],
			father_profession : [''],
			mother_profession : [''],
			how_know_about_school : [''],
			how_know_about_other : [''],
			inquiry_remark : ['',[Validators.required]],
			inquiry_handled_by : [''],
			inquirystatus : ['',[Validators.required]],
			inquiryprogress : ['',[Validators.required]],
			leadid : ['']
	    }); 
	    this.getCategories();
	    this.getCountries();
	    this.getInquiryNo();
	    this.getAcademicYears();
	    this.initOptionsMap();
		this.getLeads()

	    this.route.params.subscribe(
			params => {
				if(this.route.snapshot.params.enquiryid)
				{
					this.isLoading = true
					this.leadenableCreate = false
					this.leaddisableCreate = true
					this.commonService.getInquiryDetails(this.route.snapshot.params.enquiryid)
					.subscribe(
			 			data => {
			 			this.isLoading = false
			 			if(data['body'][0].rec_type == 'Lead'){
			 				this.rectype = true
			 				this.enquirydisable = true
			 			}
			 			else{
			 				this.rectype = false
			 				this.leaddisable = true
			 				this.lead1 = data['body'][0]['lead_no'][0]['inquiry_no'] + ' (' + data['body'][0].student_first_name + ' ' + data['body'][0].student_last_name + ')';
			 				this.leadId = data['body'][0].inquiry_master_id
			 			}
						this.inquiryno=data['body'][0].inquiry_no;
						this.inquirydate = data['body'][0].inquiry_date;	 
						this.inquirydate1=data['body'][0].inquiry_date.split('-');
						this.ngxinquirydate = {  
		                    date: {  
		                        year: parseInt(this.inquirydate1[0]),  
		                        month: parseInt(this.inquirydate1[1]),  
		                        day: parseInt(this.inquirydate1[2])
		                    },
		                }
						this.student_first_name=data['body'][0].student_first_name;
						this.student_last_name=data['body'][0].student_last_name;
						this.student_dob=data['body'][0].student_dob;
						this.student_dob1=data['body'][0].student_dob.split('-');
						this.ngxstudentdob = {  
		                    date: {  
		                        year: parseInt(this.student_dob1[2]),  
		                        month: parseInt(this.student_dob1[1]),  
		                        day: parseInt(this.student_dob1[0])
		                    },
		                }
						this.age_year=data['body'][0].age_year;

						this.age_month=data['body'][0].age_month;
						this.categoryId = data['body'][0].category_id;
						this.getCourses(this.categoryId);
						this.courseId = data['body'][0].course_id;
						if(data['body'][0].gender == 'Male'){
							this.gender = true;
						}
						else{
							this.gender = false;
						}
						if(data['body'][0].has_attended_preschool_before == 'Yes'){
							this.has_attended_preschool_before = true;
						}
						else{
							this.has_attended_preschool_before = false;
						}
						this.preschool_name = data['body'][0].preschool_name;
						this.academicyear = data['body'][0].academic_year_master_id;
						this.father_name = data['body'][0].father_name;
						this.mother_name = data['body'][0].mother_name;
						this.present_address = data['body'][0].present_address;
						this.countryId = data['body'][0].country_id;
						this.getState(this.countryId);
						this.stateId = data['body'][0].state_id;
						this.getCity(this.stateId);
						this.cityId = data['body'][0].city_id;
						this.pincode = data['body'][0].pincode;
						this.father_contact_no = data['body'][0].father_contact_no;
						if(this.mother_contact_no != undefined){
							this.mother_contact_no = data['body'][0].mother_contact_no;
						}
						if(this.father_emailid != undefined){
							this.father_emailid = data['body'][0].father_emailid;
						}
						if(this.mother_emailid != undefined){
							this.mother_emailid = data['body'][0].mother_emailid;
						}
						this.father_education = data['body'][0].father_education;
						this.mother_education = data['body'][0].mother_education;
						this.father_profession = data['body'][0].father_profession;
						this.mother_profession = data['body'][0].mother_profession;
						this.selectedCheckbox = data['body'][0].how_know_about_school;
						this.selectedOptions.push(data['body'][0].how_know_about_school);
						this.nghow_know_about_school = data['body'][0].how_know_about_school;
						if(this.selectedCheckbox == 'Other'){
							$('.other').show();
						}
						else{
							$('.other').hide();
						}
						if(this.how_know_about_other != undefined){
							this.how_know_about_other = data['body'][0].how_know_about_other;
						}
						this.inquiry_remark = data['body'][0].inquiry_remark;
						this.updated_on = data['body'][0].updated_on;
						this.inquiry_handled_by = data['body'][0].inquiry_handled_by;
						this.inquiryDate = true
						this.statusId = data['body'][0].inquiry_status
						this.progressId = data['body'][0].inquiry_progress
						this.enquiryId=params.enquiryid;




						console.log(21)
						var myTextArea = $('#myTextarea');
						var today = new Date();
				        var dd1 = today.getDate();
				        var mm1 = today.getMonth() + 1; //January is 0!
				        var yy = today.getFullYear()

				        var yyyy = today.getFullYear();
				        if (dd1 < 10) {
				            var dd = '0' + dd1;
				        } 
				        else{
				            var dd = '' + dd1
				        }
				        if (mm1 < 10) {
				            var mm = '0' + mm1;
				        } 
				        else{
				            var mm = '' + mm1
				        }
				        var prev_date = this.updated_on.split(' ')
				        if(yy +'-'+mm+'-'+dd  == prev_date[0]){
				        	console.log(1)
				        	let split_date = prev_date[0].split('-')
				        	this.remark_date = split_date[2]+'-'+split_date[1]+'-'+split_date[0] + ' ::'
				        }
				  //       else{
				  //       	console.log(myTextArea)
					 //        this.remark_date = prev_date[0]
					 //        // myTextArea.setValue('<h1>hi</h1>')
						// }

					})
					this.disableYear = true
					this.disableEnquirydate = true
				}
			}
		)

		if(this.route.snapshot.params.enquiryid == undefined){
			var myTextArea = $('#myTextarea');
			var today = new Date();
	        var dd1 = today.getDate();
	        var mm1 = today.getMonth() + 1; //January is 0!
	        var yy = today.getFullYear()

	        var yyyy = today.getFullYear();
	        if (dd1 < 10) {
	            var dd = '0' + dd1;
	        } 
	        else{
	            var dd = '' + dd1
	        }
	        if (mm1 < 10) {
	            var mm = '0' + mm1;
	        } 
	        else{
	            var mm = '' + mm1
	        }
	        this.remark_date = dd + '-' + mm + '-' +  yy +' :: '

	        	// myTextArea.val(myTextArea.val() + '</br>')
		}
		else{
		}
	}

	getLeads(){//instaed of getInquries call getLeads
		this.commonService.getILeads().subscribe((response)=>{
			if(response['status_code']==200)
		    {
		    	var arr = [];
		    	var arr_id = [];
		    	for(var i=0; i<response['body'].length; i++) {
	                var no = response['body'][i].inquiry_no ;

	                var first_name = response['body'][i].student_first_name ;
	                var last_name = response['body'][i].student_last_name ;
	                arr.push(no + ' (' + first_name + ' ' + last_name +' )');
	             }
				this.leads = arr
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getInquiryDetails(inquiryid){
		if(this.route.snapshot.params.enquiryid){
			
		}
		else{
			if(this.rectype == false && inquiryid.value != null){
				let inquirysplit = inquiryid.value.split(' ')
				this.getEnquiry = inquirysplit[0]
				this.commonService.getInquiryId(this.getEnquiry).subscribe((response)=>{

					let getInquiryId = response['body']['query_result'][0]['inquiry_master_id']
					console.log(getInquiryId)

					this.commonService.getInquiryDetails(getInquiryId).subscribe((response)=>{
						if(response['status_code'] == 200){
							this.leadId = response['body'][0].inquiry_master_id;//instaed of inquiryid 
							//get leadid
							// this.inquiryno = response['body'][0].inquiry_no;
					  //       this.academicyear = response['body'][0].academic_year_master_id;
					  //       this.inquirydate = response['body'][0].inquiry_date;	 
							// this.inquirydate1=response['body'][0].inquiry_date.split('-');
							// this.ngxinquirydate = {  
			    //                 date: {  
			    //                     year: parseInt(this.inquirydate1[0]),  
			    //                     month: parseInt(this.inquirydate1[1]),  
			    //                     day: parseInt(this.inquirydate1[2])
			    //                 },
			    //             }
							this.student_first_name = response['body'][0].student_first_name;
							this.student_last_name = response['body'][0].student_last_name;
							// this.student_dob=response['body'][0].student_dob;
							// this.student_dob1=response['body'][0].student_dob.split('-');
							// this.ngxstudentdob = {  
				   //              date: {  
				   //                  year: parseInt(this.student_dob1[2]),  
				   //                  month: parseInt(this.student_dob1[1]),  
				   //                  day: parseInt(this.student_dob1[0])
				   //              },
				   //          }
				   //          this.age_year = response['body'][0].age_year;
				   //          this.age_month = response['body'][0].age_month;
					  //       this.categoryId = response['body'][0].category_id;
							// this.getCourses(this.categoryId);
							// this.courseId = response['body'][0].course_id;
							// if(response['body'][0].gender == 'Male'){
							// 	this.gender = true;
							// }
							// else{
							// 	this.gender = false;
							// }
				   //          if(response['body'][0].has_attended_preschool_before == 'Yes'){
							// 	this.has_attended_preschool_before = true;
							// }
							// else{
							// 	this.has_attended_preschool_before = false;
							// }
							// this.preschool_name = response['body'][0].preschool_name;
							this.father_name = response['body'][0].father_name;
							// this.mother_name = response['body'][0].mother_name;
							// this.present_address = response['body'][0].present_address;
							this.countryId = response['body'][0].country_id;
							this.getState(this.countryId);
							this.stateId = response['body'][0].state_id;
							this.getCity(this.stateId);
							this.cityId = response['body'][0].city_id;
							this.pincode = response['body'][0].pincode;
							this.father_contact_no = response['body'][0].father_contact_no;
							// this.mother_contact_no = response['body'][0].mother_contact_no;
							this.father_emailid = response['body'][0].father_emailid;
							// this.mother_emailid = response['body'][0].mother_emailid;
							// this.father_profession = response['body'][0].father_profession;
							// this.mother_profession = response['body'][0].mother_profession;
							// this.father_education = response['body'][0].father_education;
							// this.mother_education = response['body'][0].mother_education;
							// this.calcDate(this.student_dob);
						}
						else{
							console.log(2)
						}
				    })
				})
			}
		}
	}

	rectypeChange(value){
		if(value == true){
			this.leadId = '';//instaed of inquiryid 
							//get leadid
			this.getInquiryNo()
			this.inquiryno = this.inquiryno;
	        this.academicyear = '2';	 
			this.student_first_name ='';
			this.student_last_name = '';
			this.father_name = '';
			this.mother_name = '';
			this.countryId = '';
			this.stateId = '';
			this.cityId ='';
			this.pincode = '';
			this.father_contact_no = '';
			this.father_emailid = '';
		}
	}

	initOptionsMap() {
	    for (var x = 0; x<this.how_know_about_school.length; x++) {
	        this.optionsMap[this.how_know_about_school[x]] = false;
	    }
	}

	updateCheckedOptions(option, event) {
		this.selectedOptions = [];
   		this.optionsMap[option] = event.target.checked;
   		this.selectedOptions.push(option);
   		if(option == 'Other' && event.target.checked == true){
   			$('.other').show();
   		}
   		else{
   			$('.other').hide();
   		}
	}	

	onEnquiryDateChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.inquirydate = event.formatted
        }
    }
    onDobChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.student_dob = event.formatted
        }
        this.calcDate(this.student_dob);
    }

    calcDate(dob){
    	if(dob != undefined){
    		var dobsplit = dob.split('-');
	    	var firstsplitlength = dobsplit[0].length
	    	if(firstsplitlength == 4){
	    		var dobdate = dobsplit[1]+'-'+dobsplit[2]+'-'+dobsplit[0];	
	    	}
	    	else{
	    		var dobdate = dobsplit[1]+'-'+dobsplit[0]+'-'+dobsplit[2];
	    	}
	    	var diff = Math.abs(new Date().getTime() - new Date(dobdate).getTime());
		    var day = 1000 * 60 * 60 * 24;
		    var days = Math.ceil(diff/day);
		    this.age_month = Math.floor(days/30);
		    console.log(this.age_month)
		    this.age_year = Math.floor(this.age_month/12);
		    if(this.age_year > 0){
		    	var yeardays = days - (365 * this.age_year);
		    	let dobmonthfloor = Math.floor(yeardays/30);
		    	this.age_month = Math.abs(dobmonthfloor)
		    }
	    }
    }

    updateOptions() {
	    for(var x in this.selectedOptions) {
	        if(this.selectedOptions[x]) {
	            this.optionsChecked.push(x);
	        }
	    }
	    this.how_know_about_school1 = this.optionsChecked;
	    this.optionsChecked = [];
	}

    OnSubmit(){
    	this.isLoading = true
    	this.updateOptions();
		this.enquiryFormSubmitted = true;
		if (this.EnquiryForm.value.has_attended_preschool_before === true) {
			this.isLoading = false
            this.EnquiryForm.get('preschool_name').setValidators([Validators.required]);

        	this.EnquiryForm.get('preschool_name').updateValueAndValidity();

        }
        else{
			this.EnquiryForm.get('preschool_name').clearValidators();
        	this.EnquiryForm.get('preschool_name').updateValueAndValidity();        	
        }
        if(this.EnquiryForm.value.rectype === false){
        	this.EnquiryForm.get('inquirydate').setValidators([Validators.required]);
			this.EnquiryForm.get('inquirydate').updateValueAndValidity()
        	this.EnquiryForm.get('student_dob').setValidators([Validators.required]);
			this.EnquiryForm.get('student_dob').updateValueAndValidity()
        	this.EnquiryForm.get('age_year').setValidators([Validators.required]);

        	this.EnquiryForm.get('age_year').updateValueAndValidity()
        	this.EnquiryForm.get('category_id').setValidators([Validators.required]);

        	this.EnquiryForm.get('category_id').updateValueAndValidity();
        	this.EnquiryForm.get('course_id').setValidators([Validators.required]);

        	this.EnquiryForm.get('course_id').updateValueAndValidity();
        	this.EnquiryForm.get('academicyear').setValidators([Validators.required]);

        	this.EnquiryForm.get('academicyear').updateValueAndValidity();
        	this.EnquiryForm.get('present_address').setValidators([Validators.required]);

        	this.EnquiryForm.get('present_address').updateValueAndValidity();
        	this.EnquiryForm.get('mother_name').setValidators([Validators.required]);

        	this.EnquiryForm.get('mother_name').updateValueAndValidity();
        	this.EnquiryForm.get('mother_contact_no').setValidators([Validators.required]);

        	this.EnquiryForm.get('mother_contact_no').updateValueAndValidity();
        	this.EnquiryForm.get('mother_emailid').setValidators([Validators.required]);

        	this.EnquiryForm.get('mother_emailid').updateValueAndValidity();
        	this.EnquiryForm.get('father_education').setValidators([Validators.required]);

        	this.EnquiryForm.get('father_education').updateValueAndValidity();
        	this.EnquiryForm.get('mother_education').setValidators([Validators.required]);

        	this.EnquiryForm.get('mother_education').updateValueAndValidity();
        	this.EnquiryForm.get('father_profession').setValidators([Validators.required]);

        	this.EnquiryForm.get('father_profession').updateValueAndValidity();
        	this.EnquiryForm.get('mother_profession').setValidators([Validators.required]);

        	this.EnquiryForm.get('mother_profession').updateValueAndValidity();
        	this.EnquiryForm.get('how_know_about_school').setValidators([Validators.required]);

        	this.EnquiryForm.get('how_know_about_school').updateValueAndValidity();

			this.EnquiryForm.value.inquirydate = this.inquirydate
			this.EnquiryForm.value.student_dob = this.student_dob
			this.EnquiryForm.value.how_know_about_school = this.selectedOptions.toString()
        	this.EnquiryForm.value.leadid = this.leadId
        }
        else{
        	this.EnquiryForm.get('inquirydate').clearValidators();
			this.EnquiryForm.get('inquirydate').updateValueAndValidity()
        	this.EnquiryForm.get('student_dob').clearValidators();
			this.EnquiryForm.get('student_dob').updateValueAndValidity()
        	this.EnquiryForm.get('age_year').clearValidators();

        	this.EnquiryForm.get('age_year').updateValueAndValidity()
        	this.EnquiryForm.get('category_id').clearValidators();

        	this.EnquiryForm.get('category_id').updateValueAndValidity();
        	this.EnquiryForm.get('course_id').clearValidators();

        	this.EnquiryForm.get('course_id').updateValueAndValidity();
        	this.EnquiryForm.get('academicyear').clearValidators();

        	this.EnquiryForm.get('academicyear').updateValueAndValidity();
        	this.EnquiryForm.get('present_address').clearValidators();

        	this.EnquiryForm.get('present_address').updateValueAndValidity();
        	this.EnquiryForm.get('mother_name').clearValidators();

        	this.EnquiryForm.get('mother_name').updateValueAndValidity();
        	this.EnquiryForm.get('mother_contact_no').clearValidators();

        	this.EnquiryForm.get('mother_contact_no').updateValueAndValidity();
        	this.EnquiryForm.get('mother_emailid').clearValidators();

        	this.EnquiryForm.get('mother_emailid').updateValueAndValidity();
        	this.EnquiryForm.get('father_education').clearValidators();

        	this.EnquiryForm.get('father_education').updateValueAndValidity();
        	this.EnquiryForm.get('mother_education').clearValidators();

        	this.EnquiryForm.get('mother_education').updateValueAndValidity();
        	this.EnquiryForm.get('father_profession').clearValidators();

        	this.EnquiryForm.get('father_profession').updateValueAndValidity();
        	this.EnquiryForm.get('mother_profession').clearValidators();

        	this.EnquiryForm.get('mother_profession').updateValueAndValidity();
        	this.EnquiryForm.get('how_know_about_school').clearValidators();

        	this.EnquiryForm.get('how_know_about_school').updateValueAndValidity();
        }
        if (this.EnquiryForm.value.how_know_about_school === 'Other') {
            this.isLoading = false
            this.EnquiryForm.get('how_know_about_other').setValidators([Validators.required]);

        	this.EnquiryForm.get('how_know_about_other').updateValueAndValidity();

        }
        else {
            this.EnquiryForm.get('how_know_about_other').clearValidators();

        	this.EnquiryForm.get('how_know_about_other').updateValueAndValidity();

        }
		if (this.EnquiryForm.value.how_know_about_school === '') {
            this.isLoading = false
            this.EnquiryForm.value.how_know_about_school = 'Website'

        }
    	console.log(this.EnquiryForm.value);
		if(this.EnquiryForm.invalid){
			this.isLoading = false
		    return;
    	}
    	this.commonService.addEditEnquiryForm(this.EnquiryForm.value,this.leadId).subscribe((response)=>{
      		this.isLoading = false
      		if(response['status_code']==200)
		    {
				this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				this.router.navigate(['/enquirylist']);
		    }
		    else
		    {
		    	this.snackbar.open(response['Metadata']['Message'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

	getCategories(){
	    this.commonService.getCategories().subscribe((response)=>{
	        this.category = response['body']
	    })
	}

	getCourses(categoryid){
	    this.commonService.getCourses(categoryid).subscribe((response)=>{
	        this.courses = response['body']
	    })
	}

	getCountries(){
	    this.commonService.getCountries().subscribe((response)=>{
	        this.countries = response['body']
	    })
	}

	getState(countryid){
		this.commonService.getState(countryid).subscribe((response)=>{
	        this.states = response['body']
	    })
	}

	getCity(stateid){
		this.commonService.getCity(stateid).subscribe((response)=>{
	        this.cities = response['body']
	    })
	}

	getInquiryNo(){
		this.commonService.getInquiryNo().subscribe((response)=>{
			if(this.route.snapshot.params.enquiryid == undefined){
	        	this.inquiryno = response['body']
	        }
	    })
	}

	getAcademicYears(){
        this.commonService.getAcademicYears().subscribe((response)=>{
            this.academicyears = response.body
        })
    }
}

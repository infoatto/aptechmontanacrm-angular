import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditenquiryComponent } from './addeditenquiry.component';

describe('AddeditenquiryComponent', () => {
  let component: AddeditenquiryComponent;
  let fixture: ComponentFixture<AddeditenquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditenquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditenquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

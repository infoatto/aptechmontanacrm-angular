import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { CommonService } from '../../../services/common.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';

@Component({
    selector: 'app-enquirylist',
    templateUrl: './enquirylist.component.html',
    styleUrls: ['./enquirylist.component.css']
})
export class EnquirylistComponent implements OnInit {

	page = 0;
	enquiryList:any = [];
	totalRecords:any = 0;
	size = 10;
    inquiryDate = "";
    academicyear : any
	enquiryFilter: any = {
    	inquiryno:"",
    	inquirydate:"",
        year: "",
	};
    isLoading = true

    myOptions: INgxMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
    };
    constructor(
    	private commonService:CommonService,
    	private snackbar:MatSnackBar,
    ) { }

    ngOnInit() {
        localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
        localStorage.removeItem('category')
        localStorage.removeItem('course')
        this.getenquiryList(1,this.enquiryFilter);
        this.getAcademicYears();
    }

    getenquiryList(page: number,enquiryFilter){
        this.commonService.listInquiries((page - 1) ,this.size,enquiryFilter).subscribe((response) => {
            console.log(response)
            this.isLoading = false
            if(response['status_code'] == 200){
                this.enquiryList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page=page
            }else{
                // this.snackbar.open(response['msg'], '', {
                //  duration : 500,
                //   verticalPosition:"top",
                //   panelClass: ['bg-red']
                // });

                this.enquiryList = [];
                this.totalRecords = 0 ;
            }
        },
        (err: any) => console.log("error",err),() =>
            console.log("getCustomersPage() retrieved customers for page:", + page)
        );
    }

    onEnquiryDateChanged(event: IMyDateModel): void 
    {
        if(event.formatted !== '') {
            this.inquiryDate = event.formatted
        }
    }

    clearfilter()
    {
        this.enquiryFilter.inquiryno=""
        this.enquiryFilter.inquirydate=""
        this.enquiryFilter.year=""
        this.getenquiryList(1,this.enquiryFilter)
    }

    onSubmit() 
    {
        this.enquiryFilter.inquirydate = this.inquiryDate
        this.getenquiryList(1,this.enquiryFilter)
    }

    getAcademicYears(){
        this.commonService.getAcademicYears().subscribe((response)=>{
            this.academicyear = response.body
        })
    }

    pageChanged(event) {
        console.log(event)
        this.getenquiryList(event, this.enquiryFilter);
    }

}

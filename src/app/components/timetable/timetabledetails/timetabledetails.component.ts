import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';
import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-timetabledetails',
  templateUrl: './timetabledetails.component.html',
  styleUrls: ['./timetabledetails.component.css']
})

@Pipe({ name: 'safe' })
export class TimetabledetailsComponent implements OnInit {

	timetable_title = ''
	cover_image = ''
	timetable_videos : any
	timetable_documents : any
	isLoading = true

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
    	private snackbar:MatSnackBar,
    	private router: Router,
    	private sanitizer: DomSanitizer
	) { }

	ngOnInit() {
		this.route.params.subscribe(
			params => {
				if(this.route.snapshot.params.timetableid)
				{
					this.commonService.viewTimetableDetails(this.route.snapshot.params.timetableid)
					.subscribe(
			 			data => { 
			 			this.isLoading = false
						if(data['status_code'] == 200){
							this.timetable_title = data['body']['timetable_details'][0]['timetable_title']
							this.cover_image = data['body']['timetable_details'][0]['cover_image_path']
							this.timetable_videos = data['body']['timetable_video']
							this.timetable_documents = data['body']['timetable_documents']
						}
						else{
							this.snackbar.open(data['msg'], '', {
								duration: 500,
								verticalPosition:"top",
								panelClass: ['bg-red']
							}); 
						}

 					})
				}
			}
		)
	}

	iframeUrl(video_url){
		return this.sanitizer.bypassSecurityTrustResourceUrl(video_url)
	}

}

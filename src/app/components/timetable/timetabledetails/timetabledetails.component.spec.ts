import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetabledetailsComponent } from './timetabledetails.component';

describe('TimetabledetailsComponent', () => {
  let component: TimetabledetailsComponent;
  let fixture: ComponentFixture<TimetabledetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetabledetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimetabledetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

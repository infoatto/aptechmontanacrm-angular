import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';

const ZOOM_STEP:number = 0.25;
const DEFAULT_ZOOM:number = 1;

@Component({
  selector: 'app-pdfview',
  templateUrl: './pdfview.component.html',
  styleUrls: ['./pdfview.component.css']
})
export class PdfviewComponent implements OnInit {

	fileURL = ''
	page: number = 1;
	totalPages: number;
	isLoaded: boolean = false;
	public pdfZoom:number = DEFAULT_ZOOM;
	isLoading = true

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
    	private snackbar:MatSnackBar,
    	private router: Router,
	) { }

	ngOnInit() {
		this.route.params.subscribe(
			params => {
				if(this.route.snapshot.params.timetabledocid)
				{
					this.commonService.viewTimeTableDoc(this.route.snapshot.params.timetabledocid)
					.subscribe(
			 			data => {
			 			this.isLoading = false
						if(data['status_code'] == 200){
							this.fileURL = data['body'][0]['doc_path']
						}
						else{
							this.snackbar.open(data['msg'], '', {
								duration: 500,
								verticalPosition:"top",
								panelClass: ['bg-red']
							}); 
						}

 					})
				}
			}
		)
	}

	afterLoadComplete(pdfData: any) {
		this.totalPages = pdfData.numPages;
		this.isLoaded = true;
	}

	nextPage() {
		this.page++;
	}

	prevPage() {
		this.page--;
	}

	zoom_out() {
		this.pdfZoom += ZOOM_STEP;
	}

	zoom_in() {
		console.log('hi')
		if (this.pdfZoom > DEFAULT_ZOOM) {
			this.pdfZoom -= ZOOM_STEP;
		}
	}

}

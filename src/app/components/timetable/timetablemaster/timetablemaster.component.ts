import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-timetablemaster',
  templateUrl: './timetablemaster.component.html',
  styleUrls: ['./timetablemaster.component.css']
})
export class TimetablemasterComponent implements OnInit {

	page = 0;
	timetableList:any = [];
	totalRecords:any = 0;
	size = 10;
	timetableFilter: any = {
	};
    isLoading = true


    constructor(
    	private commonService:CommonService,
    	private snackbar:MatSnackBar,
    ) { }

    ngOnInit() {
        localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
        localStorage.removeItem('category')
        localStorage.removeItem('course')
    	this.viewTimetables(1,this.timetableFilter);
    }

    viewTimetables(page: number,timetableFilter){
        this.commonService.viewTimetables((page - 1) ,this.size,timetableFilter).subscribe((response) => {
            this.isLoading = false
            if(response['status_code'] == 200){
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition:"top",
                    panelClass: ['bg-green']
                });
                this.timetableList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page=page
            }else{
                this.snackbar.open(response['msg'], '', {
                 duration : 500,
                  verticalPosition:"top",
                  panelClass: ['bg-red']
                });

                this.timetableList = [];
                this.totalRecords = 0 ;
            }
        },
        (err: any) => console.log("error",err),() =>
            console.log("getCustomersPage() retrieved customers for page:", + page)
        );
    }

    // clearfilter()
    // {
    //     this.timetableFilter.inquiryno=""
    //     this.timetableFilter.inquirydate=""
    //     this.timetableFilter.year=""
    //     this.viewTimetables(1,this.timetableFilter)
    // }

    onSubmit() 
    {
        this.viewTimetables(1,this.timetableFilter)
    }

    pageChanged(event) {
        this.viewTimetables(event, this.timetableFilter);
    }

}

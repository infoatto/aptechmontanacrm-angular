import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetablemasterComponent } from './timetablemaster.component';

describe('TimetablemasterComponent', () => {
  let component: TimetablemasterComponent;
  let fixture: ComponentFixture<TimetablemasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetablemasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimetablemasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

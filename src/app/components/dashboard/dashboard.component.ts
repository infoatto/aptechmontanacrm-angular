import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
        localStorage.removeItem('category')
        localStorage.removeItem('course')
	}

}

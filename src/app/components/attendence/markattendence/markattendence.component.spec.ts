import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkattendenceComponent } from './markattendence.component';

describe('MarkattendenceComponent', () => {
  let component: MarkattendenceComponent;
  let fixture: ComponentFixture<MarkattendenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkattendenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkattendenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

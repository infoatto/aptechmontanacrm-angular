import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit , NgZone} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';
import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: 'app-markattendence',
  templateUrl: './markattendence.component.html',
  styleUrls: ['./markattendence.component.css']
})
export class MarkattendenceComponent implements OnInit {

	feesselectiontype :boolean = false
	public markAttendence: FormGroup;
	categoryId = ""
	courseId = ""
	batchId = ""
	category : any
	courses : any
	batchid = ""
	groupId = ''
	groups : any
	batches : any
	isLoading = false
	public students: Array<Select2OptionData>;
	AttendenceFormSubmitted = false;
	getstudent : boolean = false
	student_single_array = []
	student_array = []

	myOptions: INgxMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
    }
    attendancedate = ''
	ngxattendancedate : any  = {  
	    jsdate: new Date()  
	};

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
		private snackbar:MatSnackBar,
		private router: Router,
        private zone: NgZone,
	) { }

	ngOnInit() {
		localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
		localStorage.removeItem('category')
		localStorage.removeItem('course')
		this.markAttendence = this.formBuilder.group({
			feesselectiontype : [''],
			categoryid : [''],
			courseid : [''],
			batchid : [''],
			groupid : [''],
			all : [''],
			single : [''],
			attendancedate : ['',[Validators.required]],
			student_array : [''],
		})
		this.getCategories()
	}

	onattendanceDateChanged(event: IMyDateModel): void 
    {
    	$('#attendancedate_error').text('')
        if(event.formatted !== '') {
            this.attendancedate = event.formatted
        }
    	this.getStudent(this.categoryId,this.courseId,this.batchId,this.groupId)
    }

	getCategories(){
	    this.commonService.getCategories().subscribe((response)=>{
	        if(response['status_code']==200)
		    {
	        	this.category = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCourses(categoryid){
	    this.commonService.getCourses(categoryid).subscribe((response)=>{
	    	if(response['status_code']==200)
		    {
	        	this.courses = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getCenterUserBatches(categoryid,courseid){
		this.commonService.getCenterUserBatches(categoryid,courseid).subscribe((response)=>{
			if(response['status_code']==200)
		    {
	        	this.batches = response['body']
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
	    })
	}

	getStudent(categoryid,courseid,batchid,groupid){
		if(this.feesselectiontype == true){
			categoryid = null
			courseid = null
			batchid = null
		}
		if(categoryid != '' && courseid != '' && batchid != ''){
			if(this.attendancedate == ''){
				$('#attendancedate_error').text('Select Attendance Date')
			}
			else{
				this.commonService.getStudentListAttendence(categoryid,courseid,batchid,groupid,this.feesselectiontype,this.attendancedate).subscribe((response)=>{
					if(response['status_code']==200)
				    {
				    	this.getstudent = true
						this.students = response['body']
				    }
				    else
				    {
				    	this.snackbar.open(response['msg'], '', {
							duration: 500,
							verticalPosition:"top",
							panelClass: ['bg-red']
						});  
				      
				    }
			    })
			}
		}
	}

	feestypeChange(type){
		if(type == true){
			this.commonService.getGroupsDD().subscribe((response)=>{
				if(response['status_code']==200)
			    {
		        	this.groups = response['body']
			    }
			    else
			    {
			    	this.snackbar.open(response['msg'], '', {
						duration: 500,
						verticalPosition:"top",
						panelClass: ['bg-red']
					});  
			      
			    }
		    })
		}
	}

	checkboxChange(type){
		for(let i=0;i<this.students.length;i++){
			if(type == true){
				let getStudentId = $('#student_'+i).val()
				this.student_array.push(getStudentId)
				$('#singlecheckbox_'+i).prop('checked',true)
			}
			else{
				this.student_array.pop();
				$('.singleChecked').prop('checked',false)
			}
		}
		this.student_array = $.unique(this.student_array)
		console.log(this.student_array)
	}

	checkboxSingleChange(index){
		if($('#singlecheckbox_'+index).is(":checked") == true){
			this.student_array.push($('#student_'+index).val())
			if(this.student_array.length == this.students.length){
				$('#checkedAll').prop('checked',true)
			}
			else{
				$('#checkedAll').prop('checked',false)
			}
		}
		else{
			console.log(this.student_array)
			var array = this.student_array
			var removeItem = $('#student_'+index).val();

			array.splice($.inArray(removeItem, array), 1);
			this.student_array = $.unique(array)
			$('#checkedAll').prop('checked',false)
		}
		console.log(this.student_array)
		
	}

	OnSubmit(){

		this.isLoading = true
		this.AttendenceFormSubmitted = true;
		if(this.feesselectiontype == false){
			this.markAttendence.value.categoryid = this.categoryId
			this.markAttendence.value.courseid = this.courseId
			this.markAttendence.value.batchid = this.batchId
        	this.isLoading = false
            this.markAttendence.get('categoryid').setValidators([Validators.required]);
        	this.markAttendence.get('categoryid').updateValueAndValidity();
            this.markAttendence.get('courseid').setValidators([Validators.required]);
        	this.markAttendence.get('courseid').updateValueAndValidity();
            this.markAttendence.get('batchid').setValidators([Validators.required]);
        	this.markAttendence.get('batchid').updateValueAndValidity();
			this.markAttendence.get('groupid').clearValidators();
        	this.markAttendence.get('groupid').updateValueAndValidity();
		}
		else{
			this.markAttendence.value.groupid = this.groupId
			this.markAttendence.get('categoryid').clearValidators();
        	this.markAttendence.get('categoryid').updateValueAndValidity();
			this.markAttendence.get('courseid').clearValidators();
        	this.markAttendence.get('courseid').updateValueAndValidity();
			this.markAttendence.get('batchid').clearValidators();
        	this.markAttendence.get('batchid').updateValueAndValidity();
            this.markAttendence.get('groupid').setValidators([Validators.required]);
        	this.markAttendence.get('groupid').updateValueAndValidity();
		}
    	if(this.student_array.length >= 1 ){
        	this.markAttendence.get('student_array').clearValidators();
        	this.markAttendence.get('student_array').updateValueAndValidity();
    	}
    	else{
    		this.markAttendence.get('student_array').setValidators([Validators.required]);
        	this.markAttendence.get('student_array').updateValueAndValidity();
			
    	}
		this.markAttendence.value.attendancedate = this.attendancedate
		if(this.markAttendence.invalid){
			this.isLoading = false
			return;
    	}
    	this.commonService.markattendence(this.markAttendence.value,this.student_array).subscribe((response)=>{
    		this.isLoading = false
      		if(response['status_code']==200)
		    {
				this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
				this.router.navigate(['/']);
		    }
		    else
		    {
		    	this.snackbar.open(response['msg'], '', {
					duration: 500,
					verticalPosition:"top",
					panelClass: ['bg-red']
				});  
		      
		    }
      		
    	})
	}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumimagesComponent } from './albumimages.component';

describe('AlbumimagesComponent', () => {
  let component: AlbumimagesComponent;
  let fixture: ComponentFixture<AlbumimagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumimagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit , NgZone} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-albumimages',
  templateUrl: './albumimages.component.html',
  styleUrls: ['./albumimages.component.css']
})
export class AlbumimagesComponent implements OnInit {

	images : any
	albumId = ''
	public ImageForm : FormGroup;
	albumpic : File
	selectedFile: File;
	isLoading = true

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
    	private snackbar:MatSnackBar,
		private formBuilder: FormBuilder,
		private router: Router,	
        private zone: NgZone,	
	) { }

	ngOnInit() {
		this.ImageForm = this.formBuilder.group({
			albumpic : ['']
		})

    	this.route.params.subscribe(
			params => {
				if(this.route.snapshot.params.albumid)
				{
					this.commonService.getAlbumsImages(this.route.snapshot.params.albumid)
					.subscribe(
			 			data => {
			 			this.isLoading = false
			 			if(data['status_code']==200)
					    {
					    	console.log(data['msg'])
							this.snackbar.open(data['msg'], '', {
								duration: 500,
								verticalPosition:"top",
								panelClass: ['bg-green']
							});
				 			this.images = data['body'];
				 			this.albumId = this.route.snapshot.params.albumid
				 		}
						else
					    {
					    	this.snackbar.open(data['msg'], '', {
								duration: 500,
								verticalPosition:"top",
								panelClass: ['bg-red']
							}); 
					      
					    }
					})
				}
			}
		)
	}

	deleteImage(albumimageid){
		this.isLoading = true 
		this.commonService.deleteAlbumsImages(albumimageid,this.albumId)
			.subscribe(
	 			data => {
	 			this.isLoading = false
	 			this.snackbar.open(data['msg'], '', {
					duration: 1000,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
	 			this.zone.runOutsideAngular(() => { 
	 				$('body').find('#main-form-content').load('index.html');
	 			});
		})
	}

	onFileChange(event:any){
		if((event.target.files[0].type=="image/jpeg")||(event.target.files[0].type=="image/jpg")||(event.target.files[0].type=="image/png"))
		{
			document.getElementById("file-name").innerHTML = event.target.files[0].name
			this.selectedFile = event.target.files[0]
		}
		else 
		{
			this.snackbar.open("Invalid file type selected. Please select .png, *.jpg,.jpeg files",'', {
				duration: 2000,
				verticalPosition:"top",
				panelClass: ['bg-red']
			});
		}
  
  	}

  	OnSubmit(){
  		this.albumpic = this.selectedFile
  		this.commonService.uploadAlbumImage(this.selectedFile,this.albumId)
			.subscribe(
	 			data => {
	 			this.snackbar.open(data['msg'], '', {
					duration: 1000,
					verticalPosition:"top",
					panelClass: ['bg-green']
				});
	 			this.zone.runOutsideAngular(() => { 
	 				
	 				$('body').find('.modal').remove();
					$('body').find('.modal-backdrop').remove();
					$('body').removeClass( "modal-open" );
	 				$('body').find('#main-form-content').load('index.html'); 
	 			});
		})
  	}

}

import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit, HostListener } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-albumlist',
  templateUrl: './albumlist.component.html',
  styleUrls: ['./albumlist.component.css']
})
export class AlbumlistComponent implements OnInit {

	array = [];
	sum = 10;
	page = 0;
	albumList:any = [];
	totalRecords:any = 0;
	size = 10;
	albumFilter: any = {
    	status:"",
	};	
    isLoading = true

	constructor(
		private commonService:CommonService,
    	private snackbar:MatSnackBar,
        private router: Router, 
	) { 
        if(this.router.url == 'albumlist'){
            window.onscroll = () => {
                let status = "not reached";
                let windowHeight = "innerHeight" in window ? window.innerHeight
                 : document.documentElement.offsetHeight;
                let body = document.body, html = document.documentElement;
                let docHeight = Math.max(body.scrollHeight,
                 body.offsetHeight, html.clientHeight,
                 html.scrollHeight, html.offsetHeight);
                let windowBottom = windowHeight + window.pageYOffset;
                if (windowBottom >= docHeight) {
                    //this.page = this.page
                    this.commonService.getAlbumsScroll(this.page).subscribe((response) => {
                        this.isLoading = false
                        if(response['status_code'] == 200){
                        }else{
                           this.snackbar.open(response['msg'], '', {
                                duration: 500,
                                verticalPosition:"top",
                                panelClass: ['bg-red']
                            });
                        }
                    },
                    (err: any) => console.log("error",err),() =>
                        console.log()
                    );
                }
          };
          }

    }

	ngOnInit() {
        localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
        localStorage.removeItem('category')
        localStorage.removeItem('course')
    	this.getAlbums(1,this.albumFilter);
	}

	getAlbums(page: number,albumFilter){
        this.commonService.getAlbums((page - 1) ,this.size,albumFilter).subscribe((response) => {
        	this.isLoading = false
            if(response['status_code'] == 200){
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition:"top",
                    panelClass: ['bg-green']
                });
                this.albumList = response['body'];
                this.page=page
            }else{
                this.snackbar.open(response['msg'], '', {
                    verticalPosition:"top",
                    panelClass: ['bg-red']
                });

                this.albumList = [];
                this.totalRecords = 0 ;
            }
        },
        (err: any) => console.log("error",err),() =>
            console.log("getCustomersPage() retrieved customers for page:", + page)
        );
    }

    clearfilter()
    {
        this.albumFilter.status=""
        this.getAlbums(1,this.albumFilter)
    }

}

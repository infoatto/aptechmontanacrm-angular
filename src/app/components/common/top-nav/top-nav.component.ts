import { Component, OnInit , NgZone} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import {MatSnackBar} from '@angular/material';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {

    
    academicyear : any
    year :any
    constructor(
    	private router: Router,
        private authService: AuthService,
        private snackbar:MatSnackBar,
        private commonService:CommonService,
        private zone: NgZone,
    ) { }
    ngOnInit() {
        this.getAcademicYears();
    }
    logout(){
        this.authService.userLogOut().subscribe((response)=>{
            console.log(response);
            if(response['status_code']==200)
            {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
            else
            {
                this.snackbar.open(response['Metadata']['Message'], '', {
                    duration: 3000,
                    verticalPosition:"top",
                    panelClass: ['bg-red']
                });  
              
            }
            
        })
    }

    getAcademicYears(){
        this.commonService.getAcademicYears().subscribe((response)=>{
            this.academicyear = response.body
            this.year = atob(localStorage.getItem('academicyearid'))
            console.log(this.academicyear)
        })
    }

    changeYear(yearid){
        localStorage.setItem('academicyearid',btoa(yearid))
        
        this.zone.runOutsideAngular(() => { $('body').find('.load_table').load('index.html'); });
    }
    

}

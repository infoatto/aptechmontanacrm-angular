import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfdocviewComponent } from './pdfdocview.component';

describe('PdfdocviewComponent', () => {
  let component: PdfdocviewComponent;
  let fixture: ComponentFixture<PdfdocviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfdocviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfdocviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

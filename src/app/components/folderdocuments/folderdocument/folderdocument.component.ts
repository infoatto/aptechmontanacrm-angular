import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-folderdocument',
  templateUrl: './folderdocument.component.html',
  styleUrls: ['./folderdocument.component.css']
})
export class FolderdocumentComponent implements OnInit {

	page = 0;
	documentList:any = [];
	totalRecords:any = 0;
	size = 10;
	documentFilter: any = {
	};
    isLoading = true

	constructor(
		private commonService:CommonService,
    	private snackbar:MatSnackBar,
	) { }

	ngOnInit() {
        localStorage.removeItem('studentid')
        localStorage.removeItem('tabindex')
        localStorage.removeItem('enquiryNO')
        localStorage.removeItem('docsave')
        localStorage.removeItem('category')
        localStorage.removeItem('course')
		this.viewDocumentFolders(1,this.documentFilter);
	}

	viewDocumentFolders(page: number,documentFilter){
        this.commonService.viewDocumentFolders((page - 1) ,this.size,documentFilter).subscribe((response) => {
            this.isLoading = false
            if(response['status_code'] == 200){
                this.snackbar.open(response['msg'], '', {
                    duration: 500,
                    verticalPosition:"top",
                    panelClass: ['bg-green']
                });
                this.documentList = response['body']['query_result'];
                this.totalRecords = response['body']['totalRecords'];
                this.page=page
            }else{
                this.snackbar.open(response['msg'], '', {
                 duration : 500,
                  verticalPosition:"top",
                  panelClass: ['bg-red']
                });

                this.documentList = [];
                this.totalRecords = 0 ;
            }
        },
        (err: any) => console.log("error",err),() =>
            console.log("getCustomersPage() retrieved customers for page:", + page)
        );
    }

    // clearfilter()
    // {
    //     this.documentFilter.inquiryno=""
    //     this.documentFilter.inquirydate=""
    //     this.documentFilter.year=""
    //     this.viewTimetables(1,this.documentFilter)
    // }

    onSubmit() 
    {
        this.viewDocumentFolders(1,this.documentFilter)
    }

    pageChanged(event) {
        this.viewDocumentFolders(event, this.documentFilter);
    }

}

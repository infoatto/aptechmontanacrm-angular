import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderdocumentComponent } from './folderdocument.component';

describe('FolderdocumentComponent', () => {
  let component: FolderdocumentComponent;
  let fixture: ComponentFixture<FolderdocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderdocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderdocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router , ActivatedRoute,Params } from '@angular/router';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import {MatSnackBar} from '@angular/material';
import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-documentdetails',
  templateUrl: './documentdetails.component.html',
  styleUrls: ['./documentdetails.component.css']
})
export class DocumentdetailsComponent implements OnInit {

	folder_details : any
	page = 0;
	totalRecords:any = 0;
	size = 10;
	documentdetailfilter: any = {
	};
	isLoading = true

	constructor(
		private route: ActivatedRoute,
		private commonService:CommonService,
		private formBuilder: FormBuilder,
    	private snackbar:MatSnackBar,
    	private router: Router,
    	private sanitizer: DomSanitizer
	) { }

	ngOnInit() {
		this.viewDocuments(1,this.documentdetailfilter);
	}

	viewDocuments(page: number,documentdetailfilter){
		this.route.params.subscribe(
			params => {
				if(this.route.snapshot.params.folderid)
				{
					this.commonService.viewFolderDocuments((page - 1) ,this.size,this.route.snapshot.params.folderid)
					.subscribe(
			 			data => {
			 			this.isLoading = false
						if(data['status_code'] == 200){
							this.folder_details = data['body']['query_result']
							this.totalRecords = data['body']['totalRecords'];
                			this.page=page
						}
						else{
							this.snackbar.open(data['msg'], '', {
								duration: 500,
								verticalPosition:"top",
								panelClass: ['bg-red']
							}); 
						}

 					},
 					(err: any) => console.log("error",err),() =>
            			console.log("getCustomersPage() retrieved customers for page:", + page)
 					)
				}
			}
		)
	}

	iframeUrl(video_url){
		return this.sanitizer.bypassSecurityTrustResourceUrl(video_url)
	}

	pageChanged(event) {
		console.log(event)
        this.viewDocuments(event, this.documentdetailfilter);
    }

}
